//
//  AppDelegate.swift
//  Uplift
//
//  Created by Harold Asiimwe on 13/10/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import IQKeyboardManagerSwift
import AWSCognitoIdentityProvider

let userPoolID = "lyx_ios_user_pool"
let awsPoolID = "us-west-2_oMEYGmPHb"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    class func defaultUserPool() -> AWSCognitoIdentityUserPool {
        return AWSCognitoIdentityUserPool(forKey: userPoolID)
    }

    var window: UIWindow?
    
    var loginViewController: SignInViewController?
    
    var resetPasswordViewController: ResetPasswordViewController?
    
    var multiFactorAuthenticationController: MultiFactorAuthenticationController?
    
    var navigationController: UINavigationController?
    
    var cognitoConfig:CognitoConfig?
    
    var accountsStoryBoard: UIStoryboard? {
        return UIStoryboard(name: "Accounts", bundle: nil)
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Use Firebase library to configure APIs
        UIApplication.shared.statusBarStyle = .lightContent
        
        AWSDDLog.sharedInstance.logLevel = .verbose
        AWSDDLog.add(AWSDDTTYLogger.sharedInstance)
        
        FirebaseApp.configure()
        
        IQKeyboardManager.shared.enable = true
        
        // Create AWSMobileClient to connect with AWS
        // setup cognito config
        self.cognitoConfig = CognitoConfig()
        
        // setup cognito
        setupCognitoUserPool()
        
        let pool:AWSCognitoIdentityUserPool = AppDelegate.defaultUserPool()
        pool.delegate = self
        
        if let currentUser = pool.currentUser(), currentUser.isSignedIn {
            //currentUser.signOutAndClearLastKnownUser()
            //currentUser.delete()
            if UserDefaults.standard.object(forKey: "IS_PATIENT") != nil {
                
                let defaults = UserDefaults.standard
                if defaults.bool(forKey: "IS_PATIENT") {
                    let storyboard = UIStoryboard(name: "Patients", bundle: nil)
                    let patientsTabBarController = storyboard.instantiateViewController(withIdentifier: "PatientsTabBarController")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = patientsTabBarController
                } else {
                    setUpPushNotificationsFirebase(application)
                }
                
            } else {
                currentUser.signOutAndClearLastKnownUser()
                signinUser()
            }
        } else {
            signinUser()
        }
        
        return true
    }
    
    var applicationStateString: String {
        if UIApplication.shared.applicationState == .active {
            return "active"
        } else if UIApplication.shared.applicationState == .background {
            return "background"
        }else {
            return "inactive"
        }
    }
    
    func setupCognitoUserPool() {
        let clientId:String = "5u1gad76eg8h8drmn5v6491d15"
        let poolId:String = "us-west-2_oMEYGmPHb"
        let clientSecret:String = "1m8e0f0c2br87skid81gejtk65q5vutqh9cfg7i99qj90rsfqs0j"
        //let region:AWSRegionType = self.cognitoConfig!.getRegion()
        
        let serviceConfiguration:AWSServiceConfiguration = AWSServiceConfiguration(region: AWSRegionType.USWest2, credentialsProvider: nil)
        let cognitoConfiguration:AWSCognitoIdentityUserPoolConfiguration = AWSCognitoIdentityUserPoolConfiguration(clientId: clientId, clientSecret: clientSecret, poolId: poolId)
        AWSCognitoIdentityUserPool.register(with: serviceConfiguration, userPoolConfiguration: cognitoConfiguration, forKey: userPoolID)
    }
    
    func signinUser() {
        let signInViewController = accountsStoryBoard?.instantiateViewController(withIdentifier: "AccountsNavigationController")
        window?.rootViewController = signInViewController
        window?.makeKeyAndVisible()
        
        let user = AppDelegate.defaultUserPool().currentUser()
        user?.getDetails().continueOnSuccessWith(block: { (task) -> Any? in
            guard task.result != nil else {
                return nil
            }
            var userAttributes:[AWSCognitoIdentityProviderAttributeType]?
            userAttributes = task.result?.userAttributes
            userAttributes?.forEach({ (attribute) in
                print("Name: " + attribute.name!)
            })
            let values = userAttributes?.filter { $0.name == "custom:is_patient" }
            let isPatient = values?.first?.value
            let nameValue = userAttributes?.filter { $0.name == "given_name" }
            let emailValue = userAttributes?.filter { $0.name == "email" }
            let allowAnswersEdit = userAttributes?.filter { $0.name == "custom:allowAnswersEdit" }
            if (isPatient != nil), Int(isPatient!)! == 1 {
                let defaults = UserDefaults.standard
                defaults.set(true, forKey: "IS_PATIENT")
                defaults.set(nameValue?.first?.value ?? "", forKey: "NAME")
                defaults.set(emailValue?.first?.value ?? "", forKey: "EMAIL")
                defaults.set(allowAnswersEdit?.first?.value ?? true, forKey: PatientsSettings.allowPatientsToEditAnswers.rawValue)
                defaults.synchronize()
                
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "Patients", bundle: nil)
                    let patientsTabBarController = storyboard.instantiateViewController(withIdentifier: "PatientsTabBarController") as! UITabBarController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = patientsTabBarController
                }
            } else {
                
                let defaults = UserDefaults.standard
                defaults.set(false, forKey: "IS_PATIENT")
                defaults.set(nameValue?.first?.value ?? "", forKey: "NAME")
                defaults.set(emailValue?.first?.value ?? "", forKey: "EMAIL")
                defaults.synchronize()
                
                DispatchQueue.main.async {
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let doctorsTabBarController = mainStoryBoard.instantiateViewController(withIdentifier: "DoctorsTabBarController") as! UITabBarController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = doctorsTabBarController
                }
            }
            return nil
        })
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    private func setUpPushNotificationsFirebase(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
}


@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // iOS10+, called when presenting notification in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        //guard let userInfo = notification.request.content.userInfo["aps"] as? NSDictionary else { return }
        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) willPresentNotification: \(userInfo)")
        //TODO: Handle foreground notification
        completionHandler([.alert])
    }
    
    // iOS10+, called when received response (default open, dismiss or custom action) for a notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) didReceiveResponse: \(userInfo)")
        //TODO: Handle background notification
        completionHandler()
    }
}

extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        NSLog("[RemoteNotification] didRefreshRegistrationToken: \(fcmToken)")
        let defaults = UserDefaults.standard
        defaults.set("\(fcmToken)", forKey: "FCM_TOKEN")
        defaults.synchronize()
        //Shared.updateAllPatientTokens(deviceToken: "\(fcmToken)")
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        NSLog("[RemoteNotification] didReceiveRegistrationToken: \(fcmToken)")
        let defaults = UserDefaults.standard
        defaults.set("\(fcmToken)", forKey: "FCM_TOKEN")
        defaults.synchronize()
        //Shared.updateAllPatientTokens(deviceToken: "\(fcmToken)")
    }
    
    // iOS9, called when presenting notification in foreground
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        NSLog("[RemoteNotification] applicationState: \(applicationStateString) didReceiveRemoteNotification for iOS9: \(userInfo)")
        if UIApplication.shared.applicationState == .active {
            //TODO: Handle foreground notification
        } else {
            //TODO: Handle background notification
        }
    }
}

extension AppDelegate: AWSCognitoIdentityInteractiveAuthenticationDelegate {
    
    func startPasswordAuthentication() -> AWSCognitoIdentityPasswordAuthentication {
        self.navigationController = nil
        self.loginViewController = nil
        if(self.navigationController == nil) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            self.navigationController = appDelegate.window?.rootViewController as? UINavigationController
            //window?.rootViewController = self.navigationController
            //window?.makeKeyAndVisible()
        }
        
        if(self.loginViewController == nil) {
            self.loginViewController = self.navigationController?.viewControllers[0] as? SignInViewController
        }
        
        DispatchQueue.main.async {
            self.navigationController!.popToRootViewController(animated: true)
            /*if(self.loginViewController!.isViewLoaded || self.loginViewController!.view.window == nil) {
                self.navigationController?.present(self.loginViewController!, animated: false, completion: nil)
            }*/
            if (!self.navigationController!.isViewLoaded
                || self.navigationController!.view.window == nil) {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController?.present(self.navigationController!,
                                                         animated: false,
                                                         completion: nil)
            }
        }
        return self.loginViewController!
    }
    
    func startNewPasswordRequired() -> AWSCognitoIdentityNewPasswordRequired {
        self.navigationController = nil
        self.resetPasswordViewController = nil
        
        if (self.resetPasswordViewController == nil) {
            self.resetPasswordViewController = self.accountsStoryBoard?.instantiateViewController(withIdentifier: "ResetPasswordController") as? ResetPasswordViewController
        }
        
        DispatchQueue.main.async {
            
            if(self.navigationController == nil) {
                self.navigationController = self.window?.rootViewController as? UINavigationController
            }
            
            if(self.resetPasswordViewController!.isViewLoaded || self.resetPasswordViewController!.view.window == nil) {
                self.navigationController?.title = "New Password"
                self.navigationController?.present(self.resetPasswordViewController!, animated: true, completion: nil)
            }
        }
        
        return self.resetPasswordViewController!
    }
    
    func startMultiFactorAuthentication() -> AWSCognitoIdentityMultiFactorAuthentication {
        if (self.multiFactorAuthenticationController == nil) {
            self.multiFactorAuthenticationController = self.accountsStoryBoard?.instantiateViewController(withIdentifier: "MultiFactorAuthenticationController") as? MultiFactorAuthenticationController
        }
        
        DispatchQueue.main.async {
            if(self.multiFactorAuthenticationController!.isViewLoaded || self.multiFactorAuthenticationController!.view.window == nil) {
                self.navigationController?.present(self.multiFactorAuthenticationController!, animated: true, completion: nil)
            }
        }
        
        return self.multiFactorAuthenticationController! as! AWSCognitoIdentityMultiFactorAuthentication
    }
}
