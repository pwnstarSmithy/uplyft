//
//  String+RegexReplace.swift
//  Uplift
//
//  Created by Harold Asiimwe on 29/03/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import Foundation
import UIKit

extension String {
    mutating func removingRegexMatches(pattern: String, replaceWith: String = "") {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
            let range = NSMakeRange(0, self.count)
            self = regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: replaceWith)
        } catch {
            return
        }
    }
}
