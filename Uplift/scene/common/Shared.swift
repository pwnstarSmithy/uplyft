//
//  Shared.swift
//  Uplift
//
//  Created by Harold Asiimwe on 13/10/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import AWSDynamoDB
import Firebase
import PKHUD

struct Shared {
    
    static func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func showAlert(title:String, message:String, viewController:UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Ok button a alert"), style: .default) { (action) in
        }
        
        alertController.addAction(OKAction)
        viewController.present(alertController, animated: true) {
        }
    }
    
    static func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    static func logout(viewController: UIViewController){
        
        if let currentUser = AppDelegate.defaultUserPool().currentUser(), currentUser.isSignedIn {
            currentUser.signOutAndClearLastKnownUser()
            if let bundleID = Bundle.main.bundleIdentifier {
                UserDefaults.standard.removePersistentDomain(forName: bundleID)
            }
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.signinUser()
            }
        }
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            //cString = cString.substring(to: cString.index(cString.startIndex, offsetBy: 1))
            cString = String(cString[cString.index(cString.startIndex, offsetBy: 1)...])
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue:  CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func retrieveOfflineAnswer(questionKey: String) -> String {
        do {
            let realm = try Realm()
            let offlineAnswer =  realm.object(ofType: OfflineAnswer.self, forPrimaryKey: questionKey)
            if let offlineAnswer = offlineAnswer {
                return offlineAnswer.answer
            }
        } catch let error {
            print(error)
        }
        return String()
    }
    
    static func saveAnswer(text: String, questionKey: String, onSuccess getKey: @escaping (_ key: String)->Void = { _ in return }) {
        let nsdateAdded = NSDate().timeIntervalSince1970
        let answer = Answer()!
        answer.uuid = NSUUID().uuidString.lowercased()
        answer.name = text
        answer.addedBy = AppDelegate.defaultUserPool().currentUser()?.username!
        answer.belongsToQuestion = questionKey
        answer.active = true
        answer.timeAdded = "\(nsdateAdded)"
        let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
        dynamoDBObjectMapper.save(answer).continueWith { (task) -> Any? in
            if task.error != nil {
                getKey("")
            } else {
                getKey(questionKey)
            }
            return nil
        }
    }
    
    static func saveNote(text: String, doctorEmail: String, patientEmail: String,
                         onSave callback: @escaping (Error?, AnyObject?)->Void ) {
        let nsdateAdded = NSDate().timeIntervalSince1970
        let note = Note()!
        note.uuid = NSUUID().uuidString.lowercased()
        note.text = text
        note.addedByDoctor = doctorEmail
        note.forPatient = patientEmail
        note.active = true
        note.timeAdded = "\(nsdateAdded)"
        let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
        dynamoDBObjectMapper.save(note).continueWith { (task) -> Any? in
            callback(task.error, task.result)
        }
    }
    
    static func saveQuestion(questionItemRef: DatabaseReference, text: String, questionText: String = "", doctorName: String, doctorEmail: String, patientEmail: String, onSave callback: @escaping (Error?, AnyObject?)->Void) {
        let nsdateAdded = NSDate().timeIntervalSince1970
        let question = Question()!
        question.uuid = NSUUID().uuidString.lowercased()
        question.name = text
        question.addedByDoctor = doctorEmail
        question.doctorName = doctorName
        question.questionText = (questionText.isEmpty ? text : questionText)
        question.belongsTo = patientEmail
        question.timeAdded = "\(nsdateAdded)"
        question.active = true
        let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
        dynamoDBObjectMapper.save(question).continueWith { (task) -> Any? in
            callback(task.error, task.result)
        }
    }
    
    static func updateAllPatientTokens(deviceToken: String) {
        let patientRef = Database.database().reference(withPath: "patients")
        patientRef.queryOrdered(byChild: "addedByDoctor").queryEqual(toValue: Auth.auth().currentUser!.email!).observe(.value, with: { snapshot in
            
            for item in snapshot.children {
                let patient = Patient(snapshot: item as! DataSnapshot)
                patient.ref?.updateChildValues(["deviceTokens": [deviceToken:true]], withCompletionBlock: { (error, reference) in
                    if error != nil {
                        print(error?.localizedDescription as Any)
                    }
                })
            }
        })
    }
    
    static func sendPushNotification(notificationData: [String: Any]) {
        let jsonData = try? JSONSerialization.data(withJSONObject: notificationData)
        let url = URL(string: "https://fcm.googleapis.com/fcm/send")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAAOSnuPSM:APA91bH0ah6ynmuD_d9dZ58cc-zsjdflPgVFxSvQwq9z15JjKrQ51kF2TaPAfcsxo3afUj5IRzpR95kvUU8BFNW8M2dJ6TulZrJQhmfn3rGscVj_9oQTLqsc_VXtXadc995EcYrsAiuh", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.httpBody = jsonData // insert json data to the request
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else { // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
        }
        task.resume()
    }
    
    static func isPasswordValid(text: String) -> Bool {
        let lowerCase = CharacterSet.lowercaseLetters
        let lowerCaseRange = text.rangeOfCharacter(from: lowerCase)
        if lowerCaseRange == nil {
            return false
        }
        let upperCase = CharacterSet.uppercaseLetters
        let upperCaseRange = text.rangeOfCharacter(from: upperCase)
        if upperCaseRange == nil {
            return false
        }
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRange = text.rangeOfCharacter(from: decimalCharacters)
        if decimalRange == nil {
            return false
        }
        return true
    }
}

