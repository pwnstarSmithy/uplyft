//
//  Question.swift
//  Uplift
//
//  Created by Harold Asiimwe on 22/10/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import Foundation
import AWSDynamoDB

@objcMembers
class Question: AWSDynamoDBObjectModel, AWSDynamoDBModeling {
    
    var uuid: String?
    var name: String?
    var questionText: String?
    var addedByDoctor: String?
    var doctorName: String?
    var belongsTo: String?
    var active:Bool = true
    var timeAdded: String?
    var isNewAnswer: Bool = false
    
    class func dynamoDBTableName() -> String {
        return "Questions"
    }
    
    class func hashKeyAttribute() -> String {
        return "uuid"
    }
    
    class func rangeKeyAttribute() -> String {
        return "timeAdded"
    }
}
