//
//  Patient.swift
//  Uplift
//
//  Created by Harold Asiimwe on 22/10/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import Foundation
import Firebase

struct Patient {
    let key: String
    let name: String
    let email: String
    let phone: String
    let password: String
    let addedByDoctor: String
    let ref: DatabaseReference?
    let deviceTokens: [String:Bool]
    let active:Bool
    let timeAdded: String
    let newAnswersAdded: [String: Bool] //Stores flag for new answers entered for a doctor in case a patient is shared across different doctors
    
    init(name: String, email: String, password: String, addedByDoctor: String, active: Bool,
         key: String = "", phone: String = "", deviceTokens: [String:Bool] = [String:Bool](), timeAdded:String, newAnswersAdded: [String: Bool] = [String:Bool]()) {
        self.key = key
        self.name = name
        self.email = email
        self.phone = phone
        self.password = password
        self.addedByDoctor = addedByDoctor
        self.active = active
        self.ref = nil
        self.deviceTokens = deviceTokens
        self.timeAdded = timeAdded
        self.newAnswersAdded = newAnswersAdded
    }
    
    init(snapshot: DataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        name = (snapshotValue["name"] as? String) ?? ""
        email = snapshotValue["email"] as! String
        phone = (snapshotValue["phone"] as? String) ?? ""
        password = snapshotValue["password"] as! String
        addedByDoctor = snapshotValue["addedByDoctor"] as! String
        active = snapshotValue["active"] as! Bool
        timeAdded = (snapshotValue["timeAdded"] as? String) ?? ""
        deviceTokens = (snapshotValue["deviceTokens"] as? [String:Bool]) ?? [String:Bool]()
        newAnswersAdded = (snapshotValue["newAnswersAdded"] as? [String:Bool]) ?? [String:Bool]()
        ref = snapshot.ref
    }
    
    func toAnyObject() -> Any {
        return [
            "name" :name,
            "email":email,
            "password": password,
            "phone": phone,
            "addedByDoctor": addedByDoctor,
            "active":active,
            "deviceTokens": deviceTokens,
            "timeAdded" : timeAdded,
            "newAnswersAdded": newAnswersAdded
        ]
    }
}
