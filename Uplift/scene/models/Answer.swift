//
//  Answer.swift
//  Uplift
//
//  Created by Harold Asiimwe on 22/10/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import Foundation
import AWSDynamoDB

@objcMembers
class Answer: AWSDynamoDBObjectModel, AWSDynamoDBModeling {
    var uuid: String?
    var name: String?
    var addedBy: String?
    var belongsToQuestion: String?
    var active:Bool = true
    var timeAdded: String?
    
    class func dynamoDBTableName() -> String {
        return "Answers"
    }
    
    class func hashKeyAttribute() -> String {
        return "uuid"
    }
    
    class func rangeKeyAttribute() -> String {
        return "timeAdded"
    }
}
