//
//  Note.swift
//  Uplift
//
//  Created by Harold Asiimwe on 01/05/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import Foundation
import AWSDynamoDB

@objcMembers
class Note: AWSDynamoDBObjectModel, AWSDynamoDBModeling {
    var uuid: String?
    var text: String?
    var addedByDoctor: String?
    var forPatient: String?
    var timeAdded: String?
    var active:Bool = true
    
    class func dynamoDBTableName() -> String {
        return "Questions"
    }
    
    class func hashKeyAttribute() -> String {
        return "uuid"
    }
    
    class func rangeKeyAttribute() -> String {
        return "timeAdded"
    }
}
