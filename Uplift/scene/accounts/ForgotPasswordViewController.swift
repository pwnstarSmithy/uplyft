//
//  ForgotPasswordViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 09/08/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider
import Moya


class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var verificationCode: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var resetPasswordButton: LoadingButton!
    
    var resetPassword = MoyaProvider<UserServices>()
    
    var emailAddress:String = ""
    var user:AWSCognitoIdentityUser?
    var uiBusy:UIActivityIndicatorView?
    
    func clearFields() {
        self.verificationCode.text = ""
        self.newPassword.text = ""
        self.confirmPassword.text = ""
        self.emailAddress = ""
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uiBusy = UIActivityIndicatorView(activityIndicatorStyle: .white)
        uiBusy?.hidesWhenStopped = true
        uiBusy?.startAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: uiBusy!)
    }
    
    func forgotPassword() {
        if emailAddress.isEmpty {
            //do something
        }else{
            
            resetPassword.request(.resetPassword(email: emailAddress)) { (result) in
                
                switch result {
                    
                case .success(let response):
                    
                    let json = try! JSONSerialization.jsonObject(with: response.data, options: [])
                    print(json)
                case .failure(let error):
                    print(error)
                }
                
            }
            
        }
        
    }
    
    func resetPasswordConfirmation(){
        
        if (self.newPassword.text == nil || self.confirmPassword.text == nil)  || (self.newPassword.text!.isEmpty || self.confirmPassword.text!.isEmpty)
            || (self.newPassword.text != self.confirmPassword.text!) {
            Shared.showAlert(title: "Reset Password Error", message: "Passwords do no match", viewController: self)
            return
        }
        
        if self.newPassword.text!.count < 8 {
            Shared.showAlert(title: "Reset Password Error", message: "Passwords must have a minimum of 8 characters", viewController: self)
            return
        }
        
        if !Shared.isPasswordValid(text: self.newPassword.text!) {
            Shared.showAlert(title: "Reset Password Error", message: "Passwords must contain a combination of upper case, lower case and number characters", viewController: self)
            return
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !emailAddress.isEmpty {
            let pool = AppDelegate.defaultUserPool()
            // Get a reference to the user using the email address
            user = pool.getUser(emailAddress)
            // Initiate the forgot password process which will send a verification code to the user
            user?.forgotPassword()
                .continueWith(block: { (response) -> Any? in
                    DispatchQueue.main.async  {
                        self.uiBusy?.stopAnimating()
                        if response.error != nil {
                            // Cannot request password reset due to error (for example, the attempt limit exceeded)
                            let alert = UIAlertController(title: "Cannot Reset Password", message: (response.error! as NSError).userInfo["message"] as? String, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                self.clearFields()
                                self.presentingViewController?.dismiss(animated: true, completion: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            // Password reset was requested and message sent.  Let the user know where to look for code.
                            let result = response.result
                            let isEmail = (result?.codeDeliveryDetails?.deliveryMedium == AWSCognitoIdentityProviderDeliveryMediumType.email)
                            let destination:String = result!.codeDeliveryDetails!.destination!
                            let medium = isEmail ? "an email" : "a text message"
                            let alert = UIAlertController(title: "Verification Sent", message: "You should receive \(medium) with a verification code at \(destination).  Enter that code here along with a new password.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                })
        }
        
    }
    
    
    @IBAction func resetPasswordPressed(_ sender: Any) {
        
//
//        resetPasswordButton.showLoading()
//        user?.confirmForgotPassword(self.verificationCode.text!, password: self.newPassword.text!)
//            .continueWith { (response) -> Any? in
//                if response.error != nil {
//                    DispatchQueue.main.async {
//                        self.resetPasswordButton.hideLoading()
//                        // The password could not be reset - let the user know
//                        let alert = UIAlertController(title: "Cannot Reset Password", message: (response.error! as NSError).userInfo["message"] as? String, preferredStyle: .alert)
//                        alert.addAction(UIAlertAction(title: "Resend Code", style: .default, handler: { (action) in
//                            self.user?.forgotPassword()
//                                .continueWith(block: { (result) -> Any? in
//                                    print("Code Sent")
//                                    return nil
//                                })
//                        }))
//                        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
//                            self.dismiss(animated: true, completion: nil)
//                        }))
//                        self.present(alert, animated: true, completion: nil)
//                    }
//                } else {
//                    // Password reset.  Send the user back to the login and let them know they can login with new password.
//                    DispatchQueue.main.async {
//                        self.resetPasswordButton.hideLoading()
//                        print("Password set successfully!!")
//                        let presentingController = self.presentingViewController
//                        //self.dismiss(animated: <#T##Bool#>, completion: <#T##(() -> Void)?##(() -> Void)?##() -> Void#>)
//                        self.dismiss(animated: true, completion: {
//                            let alert = UIAlertController(title: "Password Reset", message: "Password reset.  Please log into the account with your email and new password.", preferredStyle: .alert)
//                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                            presentingController?.present(alert, animated: true, completion: nil)
//                            self.clearFields()
//                        }
//                        )
//                    }
//                }
//                return nil
//        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
