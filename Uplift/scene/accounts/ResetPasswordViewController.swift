//
//  ResetPasswordViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 24/06/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider

class ResetPasswordViewController: UIViewController {
    
    @IBOutlet weak var newPasswordInput: UITextField!
    @IBOutlet weak var confirmNewPasswordInput: UITextField!
    @IBOutlet weak var submitButton: LoadingButton!
    
    var currentUserAttributes:[String:String]?
    
    var resetPasswordCompletion: AWSTaskCompletionSource<AWSCognitoIdentityNewPasswordRequiredDetails>?

    @IBAction func resetCancelButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.newPasswordInput.text = nil
        self.newPasswordInput.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
    }
    
    @objc func inputDidChange(_ sender:AnyObject) {
        if (self.newPasswordInput.text != nil && self.confirmNewPasswordInput != nil) {
            self.submitButton.isEnabled = true
        } else {
            self.submitButton.isEnabled = false
        }
    }
    
    
    @IBAction func submitNewPassword(_ sender: Any) {
        if (self.newPasswordInput.text == nil || self.confirmNewPasswordInput.text == nil)  || (self.newPasswordInput.text!.isEmpty || self.confirmNewPasswordInput.text!.isEmpty)
            || (self.newPasswordInput.text != self.confirmNewPasswordInput.text!) {
            Shared.showAlert(title: "Reset Password Error", message: "Passwords do no match", viewController: self)
            return
        }
        if self.newPasswordInput.text!.count < 8 {
            Shared.showAlert(title: "Reset Password Error", message: "Passwords must have a minimum of 8 characters", viewController: self)
            return
        }
        
        if !Shared.isPasswordValid(text: self.newPasswordInput.text!) {
            Shared.showAlert(title: "Reset Password Error", message: "Passwords must contain a combination of upper case, lower case and number characters", viewController: self)
            return
        }
        
        let details = AWSCognitoIdentityNewPasswordRequiredDetails(proposedPassword: self.newPasswordInput.text!, userAttributes: [:])
        if let isSet = self.resetPasswordCompletion?.trySet(result: details) {
            if !isSet {
                self.dismiss(animated: true)
            } else {
                submitButton.showLoading()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ResetPasswordViewController: AWSCognitoIdentityNewPasswordRequired {
    func getNewPasswordDetails(_ newPasswordRequiredInput: AWSCognitoIdentityNewPasswordRequiredInput, newPasswordRequiredCompletionSource: AWSTaskCompletionSource<AWSCognitoIdentityNewPasswordRequiredDetails>) {
        self.currentUserAttributes = newPasswordRequiredInput.userAttributes
        self.resetPasswordCompletion = newPasswordRequiredCompletionSource
    }
    
    func didCompleteNewPasswordStepWithError(_ error: Error?) {
        DispatchQueue.main.async {
            self.submitButton.hideLoading()
            if let error = error as NSError? {
                let alert = UIAlertController(title: "Reset Password Error",
                                                        message: error.userInfo["message"] as? String,
                                                        preferredStyle: .alert)
                let retryAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(retryAction)
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alert.addAction(cancel)
                
                self.present(alert, animated: true, completion: nil)
            } else {
                self.newPasswordInput.text = nil
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.signinUser()
                }
            }
        }
    }
}
