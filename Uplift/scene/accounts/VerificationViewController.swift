//
//  VerificationViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 29/06/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider

class VerificationViewController: UIViewController {

    @IBOutlet weak var resetButton: LoadingButton!
    @IBOutlet weak var verifyButton: LoadingButton!
    @IBOutlet weak var verificationField: UITextField!
    @IBOutlet weak var verificationLabel: UILabel!
    
    var codeDeliveryDetails:AWSCognitoIdentityProviderCodeDeliveryDetailsType?
    
    var user: AWSCognitoIdentityUser?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let isEmail = (codeDeliveryDetails?.deliveryMedium == AWSCognitoIdentityProviderDeliveryMediumType.email)
        let medium = isEmail ? "your email address" : "your phone number"
        let destination = codeDeliveryDetails!.destination!
        verificationLabel.text = "Please enter the code that was sent to \(medium) at \(destination)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func resetConfirmation(message:String? = "") {
        self.verificationField.text = ""
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { action in self.resendConfirmation(self) }))
        self.present(alert, animated: true, completion:nil)
    }
    
    @IBAction func confirm(_ sender: Any) {
        verifyButton.showLoading()
        self.user?.confirmSignUp(self.verificationField.text!, forceAliasCreation: true).continueWith {[weak self] (task: AWSTask) -> AnyObject? in
            guard let strongSelf = self else { return nil }
            DispatchQueue.main.async(execute: {
                strongSelf.verifyButton.hideLoading()
                if let error = task.error as NSError? {
                    strongSelf.resetConfirmation(message: (error as NSError).userInfo["message"] as? String)
                } else {
                    
                    let alertController = UIAlertController(title: "Account verification",
                                                            message: "Your email has been successfully verified",
                                                            preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: { action in
                        strongSelf.navigationController?.popToRootViewController(animated: true)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dimissOnEmailVerification"), object: nil)
                    })
                    alertController.addAction(okAction)
                    
                    self?.present(alertController, animated: true, completion:  nil)
                }
            })
            return nil
        }
    }
    
    
    @IBAction func resendConfirmation(_ sender: Any) {
        resetButton.showLoading()
        self.user?.resendConfirmationCode().continueWith {[weak self] (task: AWSTask) -> AnyObject? in
            guard let _ = self else { return nil }
            DispatchQueue.main.async(execute: {
                self?.resetButton.hideLoading()
                if let error = task.error as NSError? {
                    let alertController = UIAlertController(title: error.userInfo["__type"] as? String,
                                                            message: error.userInfo["message"] as? String,
                                                            preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alertController.addAction(okAction)
                    
                    self?.present(alertController, animated: true, completion:  nil)
                } else if let result = task.result {
                    let alertController = UIAlertController(title: "Code Resent",
                                                            message: "Code resent to \(result.codeDeliveryDetails?.destination! ?? " no message")",
                        preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alertController.addAction(okAction)
                    self?.present(alertController, animated: true, completion: nil)
                }
            })
            return nil
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
