//
//  SignInViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 13/10/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import UIKit
//import Firebase
import AWSCognitoIdentityProvider
import PKHUD
import Moya

class SignInViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signinButton: LoadingButton!
    
    //var patientRef = DatabaseReference()
    
    var userLogin = MoyaProvider<UserServices>()
    
    var passwordAuthenticationCompletion: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>?
    
    var user:AWSCognitoIdentityUser?
    var userAttributes:[AWSCognitoIdentityProviderAttributeType]?
    
    @IBAction func signButtonTapped(_ sender: Any) {
//        if (self.emailTextField?.text != nil && self.passwordTextField?.text != nil) {
//            signinButton.showLoading()
//            let authDetails = AWSCognitoIdentityPasswordAuthenticationDetails(username: self.emailTextField!.text!,
//                                                                              password: self.passwordTextField!.text! )
//            self.passwordAuthenticationCompletion?.set(result: authDetails)
//        }
        
        loginUser()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("sign in view has been loaded.")
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        //self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.passwordTextField?.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        self.emailTextField?.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func loginUser() {
        
        let email : String?
        let password : String?
        
        
        if (self.emailTextField?.text != nil && self.passwordTextField?.text != nil) {
            signinButton.showLoading()
            
            email = emailTextField.text
            password = passwordTextField.text
            
            userLogin.request(.login(email: email!, password: password!)) { (result) in
                
                switch result {
                    
                case .success(let response):
                
                    let json = try! JSONSerialization.jsonObject(with: response.data, options: [])
                    print(json)
                    
                case .failure(let error):
                    print(error)
                
                }
                
            }
        }
        

        
        
    }
    
    @objc func inputDidChange(_ sender:AnyObject) {
        if (self.emailTextField?.text != nil && self.passwordTextField?.text != nil) {
            self.signinButton?.isEnabled = true
        } else {
            self.signinButton?.isEnabled = false
        }
    }

    
    // MARK: - Navigation
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "forgotPasswordSegue" {
            if let email = emailTextField.text, email.isEmpty {
                Shared.showAlert(title: "Forgot Password", message: "Please enter your email address to proceed", viewController: self)
                return false
            }
        }
        return true
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "forgotPasswordSegue" {
            let forgotPasswordViewController = segue.destination as! ForgotPasswordViewController
            forgotPasswordViewController.emailAddress = emailTextField.text ?? ""
        }
    }
    

}

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        }
        if textField == emailTextField {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}

extension SignInViewController: AWSCognitoIdentityPasswordAuthentication {
    func getDetails(_ authenticationInput: AWSCognitoIdentityPasswordAuthenticationInput, passwordAuthenticationCompletionSource: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>) {
        self.passwordAuthenticationCompletion = passwordAuthenticationCompletionSource
        DispatchQueue.main.async {
            if (self.emailTextField?.text == nil) {
                self.emailTextField?.text = authenticationInput.lastKnownUsername
            }
        }
    }
    
    func didCompleteStepWithError(_ error: Error?) {
        DispatchQueue.main.async {
            self.signinButton.hideLoading()
            if let error = error as NSError? {
                let alertController = UIAlertController(title: "Sign-in Error",
                                                        message: error.userInfo["message"] as? String,
                                                        preferredStyle: .alert)
                let retryAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(retryAction)
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alertController.addAction(cancel)
                
                self.present(alertController, animated: true, completion: nil)
            } else {
                self.user = AppDelegate.defaultUserPool().currentUser()
                
                self.dismiss(animated: true, completion: {
                })
            }
        }
    }
    
    
}
