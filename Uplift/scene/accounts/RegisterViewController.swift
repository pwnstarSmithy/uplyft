//
//  RegisterViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 13/10/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider
import Moya
class RegisterViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: LoadingButton!
    
    var user: AWSCognitoIdentityUser?
    var codeDeliveryDetails:AWSCognitoIdentityProviderCodeDeliveryDetailsType?
    
    var registerUser = MoyaProvider<UserServices>()
    
    @IBAction func cancelBarButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerButtonTapped(_ sender: Any) {

        userReg()
        
        let userPool = AppDelegate.defaultUserPool()
        registerButton.showLoading()
        let isPatient = AWSCognitoIdentityUserAttributeType(name: "custom:is_patient", value: "0")
        let attributes:[AWSCognitoIdentityUserAttributeType] = [isPatient]
        userPool.signUp(emailTextField.text!, password: passwordTextField.text!, userAttributes: attributes, validationData: nil)
            .continueWith { (response) -> Any? in
                DispatchQueue.main.async {
                    self.registerButton.hideLoading()
                    if response.error != nil {
                        // Error in the Signup Process
                        let alert = UIAlertController(title: "Registration Error", message: (response.error! as NSError).userInfo["message"] as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.user = response.result!.user
                        // Does user need confirmation?
                        if (response.result?.userConfirmed?.intValue != AWSCognitoIdentityUserStatus.confirmed.rawValue) {
                            // User needs confirmation, so we need to proceed to the verify view controller
                            self.codeDeliveryDetails = response.result?.codeDeliveryDetails
                            self.performSegue(withIdentifier: "VerifySegue", sender: self)
                        } else {
                            // User signed up but does not need confirmation.  This should rarely happen (if ever).
                            self.presentingViewController?.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                return nil
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.cancelBarButtonTapped(_:)),
                                               name: NSNotification.Name(rawValue: "dimissOnEmailVerification"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationItem.title = "Register"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func userReg(){
        
        let email : String?
        let password : String?
        
        passwordTextField.resignFirstResponder()
        if emailTextField.text!.isEmpty || passwordTextField.text!.isEmpty {
            Shared.showAlert(title: "Register", message: "Please enter all the fields", viewController: self)
            return
        }
        
        if !Shared.isValidEmail(testStr: emailTextField.text!) {
            Shared.showAlert(title: "Register", message: "Please enter a valid email address", viewController: self)
            return
        }
        
        if passwordTextField.text!.count < 8{
            Shared.showAlert(title: "Register", message: "Password field should at least have 8 characters", viewController: self)
            return
        }
        
        if !Shared.isPasswordValid(text: passwordTextField.text!) {
            Shared.showAlert(title: "Register",
                             message: "Passwords must contain a combination of upper case, lower case and number characters", viewController: self)
            return
        }
        
        email = emailTextField.text
        password = passwordTextField.text
        
        registerUser.request(.register(email: email!, password: password!)) { (result) in
            
            switch result {
                
            case .success(let response):
                
                let json = try! JSONSerialization.jsonObject(with: response.data, options: [])
                print(json)
                
                let alert = UIAlertController(title: "Email Verification",
                                              message: "Account created, please check your email for verification code",
                                              preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alert.addAction(cancel)

            case .failure(let error):
                
                let alert = UIAlertController(title: "Error",
                                              message: "\(error)",
                                              preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
            }
            
            
        }
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let verificationController = segue.destination as! VerificationViewController
        verificationController.codeDeliveryDetails = self.codeDeliveryDetails
        verificationController.user = self.user!
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "dimissOnEmailVerification"), object: nil)
    }
}
