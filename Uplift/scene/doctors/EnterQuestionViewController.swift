//
//  EnterQuestionViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 02/11/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import UIKit
import Firebase
import AWSDynamoDB
import GrowingTextView
import PKHUD

class EnterQuestionViewController: UIViewController, UITextViewDelegate, GrowingTextViewDelegate {
    
    var questionRef = DatabaseReference()
    var patientEmail = ""
    var doctorEmail  = ""
    var doctorName   = ""
    var questionToBeEdited: Question!
    var isAbilityToAddCustomPlaceHolderForNewQtnEnabled = false
    var tempEnteredQuestionText = ""
    var questionSavedDelegate: QuestionSavedDelegate?
    var placeHolderEditedText = ""
    var editedQuestionText = ""
    
    @IBOutlet weak var enteredQuestion: GrowingTextView!
    
    @IBAction func saveEnteredQuestion(_ sender: Any) {
        if enteredQuestion.isFirstResponder {
            enteredQuestion.resignFirstResponder()
        }
        if enteredQuestion.text.isEmpty {
            Shared.showAlert(title: "Enter Question", message: "No question text was entered", viewController: self)
            return
        }
        
        if questionToBeEdited != nil {
            if !(questionToBeEdited.name?.isEmpty)! && placeHolderEditedText.isEmpty && editedQuestionText.isEmpty {
                showAlertToConfirmPlaceHolderEdit()
            } else {
                placeHolderEditedText = enteredQuestion.text
                saveQuestionEdit(text: editedQuestionText, placeHolderEditText: placeHolderEditedText)
            }
            //saveQuestionEdit(text: enteredQuestion.text)
        } else if !tempEnteredQuestionText.isEmpty {
            saveQuestion(text: tempEnteredQuestionText, questionText: enteredQuestion.text)
        } else {
            saveQuestion(text: enteredQuestion.text)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            //scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        // Do any additional setup after loading the view.
        questionRef = Database.database().reference(withPath: "questions")
        questionRef.keepSynced(true)
        
        self.hideKeyboardWhenTappedAround()
        
        if questionToBeEdited != nil {
            enteredQuestion.text = (questionToBeEdited.name?.isEmpty)! ? questionToBeEdited.questionText : questionToBeEdited.name
            isAbilityToAddCustomPlaceHolderForNewQtnEnabled = false
        } else { //we are entering a new question so add ability to enter a custom place holder after a question has been entered into the text view
            isAbilityToAddCustomPlaceHolderForNewQtnEnabled = true
            enteredQuestion.placeholder = "Enter Question here..."
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.view.endEditing(true)
        //saveEnteredQuestion(textView)
        if !enteredQuestion.text.isEmpty && isAbilityToAddCustomPlaceHolderForNewQtnEnabled {
            // save the question text first and then switch the textview to enable entering placeholder text
            tempEnteredQuestionText = enteredQuestion.text
            showRightBarButton()
        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    func saveQuestion(text: String, questionText: String = "") {
        HUD.show(.progress)
        Shared.saveQuestion(questionItemRef: self.questionRef, text: text, questionText: questionText, doctorName: (doctorName.isEmpty ? doctorEmail : doctorName), doctorEmail: doctorEmail, patientEmail: patientEmail) { (error, ref) in
            DispatchQueue.main.async  {
                HUD.hide()
                if error == nil {
                    self.questionSavedDelegate?.onQuestionSaved()
                    self.navigationController?.popViewController(animated: true)
                } else {
                    Shared.showAlert(title: "New Question Error", message: (error?.localizedDescription)!, viewController: self)
                }
            }
        }
    }
    
    func saveQuestionEdit(text: String, placeHolderEditText: String = "") {
        if let question = questionToBeEdited {
            question.name = text
            !placeHolderEditedText.isEmpty ? question.questionText = placeHolderEditedText : ()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
            dynamoDBObjectMapper.save(question).continueWith { (task) -> Any? in
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if let error = task.error as NSError? {
                        print("The request failed. Error: \(error)")
                        Shared.showAlert(title: "Error", message: (error.localizedDescription), viewController: self)
                    } else {
                        self.questionSavedDelegate?.onQuestionSaved()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    func showRightBarButton() {
        let rightBarButton = UIBarButtonItem(title: "Add placeholder", style: .plain, target: self, action: #selector(startEnteringPlaceHolder))
        navigationItem.rightBarButtonItem = rightBarButton
    }

    @objc func startEnteringPlaceHolder() {
        // change the textview place holder text to change place holder.
        enteredQuestion.text = ""
        enteredQuestion.placeholder = "Enter placeholder answer for the previous question entered."
        navigationItem.rightBarButtonItem = nil
        isAbilityToAddCustomPlaceHolderForNewQtnEnabled = false
    }
    
    func showAlertToConfirmPlaceHolderEdit() {
        let alertController = UIAlertController(title: title, message: "Would you like to edit the placeholder text?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Ok button a alert"), style: .default) { (action) in
            self.editedQuestionText = self.enteredQuestion.text
            self.enteredQuestion.text = self.questionToBeEdited.questionText
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel button a alert"), style: .cancel) { (action) in
            self.saveQuestionEdit(text: self.enteredQuestion.text)
        }
        
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true) {
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
