//
//  TypeNoteViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 01/05/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSDynamoDB
import GrowingTextView
import PKHUD

class TypeNoteViewController: UIViewController {
    
    var patientEmail = ""
    var doctorEmail  = ""
    var saveNoteDelegate: SaveNotesDelegate?
    var noteToEdit: Note?

    @IBOutlet weak var noteTextView: GrowingTextView!
    
    @IBAction func saveEnteredNoteText(_ sender: Any) {
        
        if noteTextView.isFirstResponder {
            noteTextView.resignFirstResponder()
        }
        if noteTextView.text.isEmpty {
            Shared.showAlert(title: "Enter Note", message: "Please enter note to save", viewController: self)
            return
        }
        if noteToEdit != nil { saveNoteEdit(text:  noteTextView.text)} else { saveNote(text: noteTextView.text) }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        navigationItem.title = "New Note"
        if let noteToEdit = noteToEdit {
            noteTextView.text = noteToEdit.text
        } else {
            noteTextView.placeholder = "Enter note here..."
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveNote(text: String) {
        HUD.show(.progress)
        Shared.saveNote(text: text, doctorEmail: doctorEmail, patientEmail: patientEmail) { (error, ref) in
            DispatchQueue.main.async {
                HUD.hide()
                if error == nil {
                    self.saveNoteDelegate?.onNoteSaved()
                    self.navigationController?.popViewController(animated: true)
                } else {
                    Shared.showAlert(title: "New Note Error", message: (error?.localizedDescription)!, viewController: self)
                }
            }
        }
    }
    
    func saveNoteEdit(text: String) {
        if let noteToEdit = noteToEdit {
            noteToEdit.text = text
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
            dynamoDBObjectMapper.save(noteToEdit).continueWith { (task) -> Any? in
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if let error = task.error as NSError? {
                        print("The request failed. Error: \(error)")
                        Shared.showAlert(title: "Error", message: (error.localizedDescription), viewController: self)
                    } else {
                        self.saveNoteDelegate?.onNoteSaved()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
