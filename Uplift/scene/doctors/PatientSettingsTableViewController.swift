//
//  PatientSettingsTableViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 10/5/18.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider
import PKHUD

enum PatientsSettings: String {
    case allowPatientsToEditAnswers = "allowPatientsToEditAnswers"
}

class PatientSettingsTableViewController: UITableViewController {
    
    let defaults = UserDefaults.standard
    var awsCognitoIdentityProvider:AWSCognitoIdentityProvider?
    
    var failedCounterMap = [String:Int]()
    var failedCounter   = 0
    var submittedPatients = 0
    
    @IBOutlet weak var allowEditableAnswersLabel: UILabel!
    @IBOutlet weak var editableAnswersSwitch: UISwitch!
    
    @objc func editableAnswersSwitchToggled(_ sender: Any) {
        if allPatients.count < 1 {
            let alertController = UIAlertController(title: "Patient Settings", message:  "No patients were found", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Ok button a alert"), style: .default) { (action) in
                if self.editableAnswersSwitch.isOn {
                    self.editableAnswersSwitch.setOn(false, animated: true)
                } else {
                    self.editableAnswersSwitch.setOn(true, animated: true)
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true) {}
        } else {
            HUD.show(.progress)
            submittedPatients = 0
            failedCounter = 0
            failedCounterMap.removeAll(keepingCapacity: false)
            allPatients.forEach { (patient) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                let req = AWSCognitoIdentityProviderAdminUpdateUserAttributesRequest()!
                req.username = patient.username!
                req.userPoolId = awsPoolID
                req.userAttributes = [AWSCognitoIdentityUserAttributeType(name: "custom:allowAnswersEdit", value: "\(editableAnswersSwitch.isOn ? 1 : 0)")]
                awsCognitoIdentityProvider?.adminUpdateUserAttributes(req, completionHandler: { (response, error) in
                    if error == nil {
                        self.submittedPatients = self.submittedPatients + 1
                        if self.submittedPatients == allPatients.count {
                            DispatchQueue.main.async {
                                HUD.flash(.success, delay: 2.0)
                                self.defaults.set(self.editableAnswersSwitch.isOn, forKey: PatientsSettings.allowPatientsToEditAnswers.rawValue)
                                self.defaults.synchronize()
                            }
                        }
                    } else {
                        self.failedCounter = self.failedCounter + 1
                        self.failedCounterMap = [patient.username!: self.failedCounter ]
                        if self.submittedPatients == allPatients.count {
                            DispatchQueue.main.async {
                                HUD.hide()
                                Shared.showAlert(title: "Patient Settings", message: "\(self.failedCounter) patients were not updated", viewController: self)
                            }
                        }
                        print("The request failed. Error: \(String(describing: error))")
                    }
                })
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        awsCognitoIdentityProvider = AWSCognitoIdentityProvider.default()
        editableAnswersSwitch.addTarget(self, action: #selector(editableAnswersSwitchToggled), for: .valueChanged)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.editableAnswersSwitch.setOn(defaults.bool(forKey: PatientsSettings.allowPatientsToEditAnswers.rawValue), animated: true)
    }

    // MARK: - Table view data source

    /*override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }*/

    /*override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
