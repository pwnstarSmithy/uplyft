//
//  QuestionsTableViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 02/11/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSDynamoDB
import SwiftDate

protocol QuestionSavedDelegate {
    func onQuestionSaved()
}

class AskedQuestionsViewController: UIViewController {

    var refreshControl = UIRefreshControl()
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var selectedSegmentControlIndex = 0
    var items: [Question] = []
    var allQuestions: [Question] = []
    var patientUuid = ""
    var patientName  = ""
    var patientEmail = ""
    var doctorEmail  = ""
    var doctorName   = ""
    var selectedQuestion: Question!
    var questionCatergory : QuestionCategory!
    var questionsToAsk = [(name:String, value:String)]()
    
    
    @IBAction func segmentControlValueChanged(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex {
        case 1:
            selectedSegmentControlIndex = 1
            reloadTableView(active: false)
        default:
            selectedSegmentControlIndex = 0
            reloadTableView(active: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addQuestionButton()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        refreshControl.attributedTitle = NSAttributedString(string: "Reload")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        getQuestions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.shadowImage = #imageLiteral(resourceName: "default_color_pixel")
        navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "default_color_pixel"), for: .default)
        navigationItem.title = patientName
        selectedQuestion = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        getQuestions()
    }
    
    private func getQuestions() {
        let scanExpression = AWSDynamoDBScanExpression()
        scanExpression.filterExpression = "addedByDoctor = :doc AND belongsTo = :patient"
        scanExpression.expressionAttributeValues = [":doc" : doctorEmail, ":patient" : patientEmail]
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
        dynamoDBObjectMapper.scan(Question.self, expression: scanExpression).continueWith { (task) -> Any? in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.refreshControl.endRefreshing()
                if let error = task.error as NSError? {
                    print("The request failed. Error: \(error)")
                } else if let paginatedOutput = task.result {
                    self.allQuestions =  paginatedOutput.items as! [Question]
                    self.allQuestions = self.allQuestions.sorted(by: { (qtn1, qtn2) -> Bool in
                        Double(qtn1.timeAdded!)! > Double(qtn2.timeAdded!)!
                    })
                    if self.selectedSegmentControlIndex == 1 {
                        self.reloadTableView(active: false)
                    } else {
                        self.reloadTableView(active: true)
                    }
                }
            }
        }
    }
    
    func reloadTableView(active: Bool) {
        items = allQuestions
        items = items.filter { $0.active == active }
        tableView.reloadData()
        if self.items.count > 0 {
            self.tableView.backgroundView = nil
        }
    }
    
    func addQuestionButton() {
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addQuestion))
        let secondRightBarButton = UIBarButtonItem(image: UIImage(named: "notes"), style: .plain, target: self, action: #selector(goToNotes))
        navigationItem.rightBarButtonItems = [rightBarButton, secondRightBarButton]
    }
    
    @objc func addQuestion() {
        //performSegue(withIdentifier: "enterQuestionSegue", sender: self)
        questionType()
    }
    
    @objc func goToNotes() {
        performSegue(withIdentifier: "goToNotesSegue", sender: self)
    }
    
    private func questionType() {
        let alertController = UIAlertController(title: "Select question category", message: nil, preferredStyle: .actionSheet)
        
        let thoughtRecordButton = UIAlertAction(title: "Thought Record Sheet", style: .default, handler: { (action) -> Void in
            self.questionsToAsk = AskedQuestionsManager().getUnSelectedQuestions(questionCatergory: .thoughtRecord, questions: self.items)
            if self.questionsToAsk.count > 0 {
                self.questionCatergory = QuestionCategory.thoughtRecord
                self.performSegue(withIdentifier: "selectQuestionSegue", sender: self)
            } else {
                Shared.showAlert(title: "Thought Record", message: "The selected question category has already been asked", viewController: self)
            }
        })
        
        let  avoidanceSufferingButton = UIAlertAction(title: "Avoidance & suffering diary", style: .default, handler: { (action) -> Void in
            self.questionsToAsk = AskedQuestionsManager().getUnSelectedQuestions(questionCatergory: .avoidanceSufferingDiary, questions: self.items)
            if self.questionsToAsk.count > 0 {
                self.questionCatergory = QuestionCategory.avoidanceSufferingDiary
                self.performSegue(withIdentifier: "selectQuestionSegue", sender: self)
            } else {
                Shared.showAlert(title: "Avoidance & suffering diary", message: "The selected question category has already been asked", viewController: self)
            }
        })
        
        let  newQuestionButton = UIAlertAction(title: "New question", style: .default, handler: { (action) -> Void in
            self.performSegue(withIdentifier: "enterQuestionSegue", sender: self)
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        alertController.addAction(thoughtRecordButton)
        alertController.addAction(avoidanceSufferingButton)
        alertController.addAction(newQuestionButton)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func removeUnreadFlag(indexPath: IndexPath) {
        let question = items[indexPath.row]
        question.isNewAnswer = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
        dynamoDBObjectMapper.save(question).continueWith { (task) -> Any? in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let error = task.error as NSError? {
                    print("The request failed. Error: \(error)")
                }
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "enterQuestionSegue" {
            let enterQuestionVC = segue.destination as! EnterQuestionViewController
            enterQuestionVC.doctorEmail = self.doctorEmail
            enterQuestionVC.patientEmail = self.patientEmail
            enterQuestionVC.doctorName = self.doctorName
            enterQuestionVC.questionSavedDelegate = self
            if selectedQuestion != nil {
                enterQuestionVC.questionToBeEdited = selectedQuestion
            }
            //print(selectedQuestion)
        } else if segue.identifier == "showAnswerSegue" {
            let answerSegue = segue.destination as! AnsweredQuestionViewController
            answerSegue.selectedQuestion = selectedQuestion
            answerSegue.patientEmail = self.patientUuid
        } else if segue.identifier == "selectQuestionSegue" {
            let selectQuestionVC = segue.destination as! SelectQuestionTableViewController
            selectQuestionVC.questionCategory = questionCatergory
            selectQuestionVC.doctorEmail = self.doctorEmail
            selectQuestionVC.patientEmail = self.patientEmail
            selectQuestionVC.doctorName = self.doctorName
            selectQuestionVC.questionSavedDelegate = self
            switch questionCatergory {
            case .thoughtRecord?:
                selectQuestionVC.questionItems = self.questionsToAsk.compactMap({ (key, value) -> String? in
                    return key
                })
            case .avoidanceSufferingDiary?:
                selectQuestionVC.questionItems = self.questionsToAsk.compactMap({ (key, value) -> String? in
                    return value
                })
            default: ()
            }
        } else if segue.identifier == "goToNotesSegue" {
            let notesVC = segue.destination as! NotesViewController
            notesVC.addByDoctor = self.doctorEmail
            notesVC.forPatient = self.patientEmail
        }
    }

}

extension AskedQuestionsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedQuestion = items[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        removeUnreadFlag(indexPath: indexPath)
        performSegue(withIdentifier: "showAnswerSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let question = self.items[indexPath.row]
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            print("Edit action received at \(indexPath.row)")
            self.selectedQuestion = self.items[indexPath.row]
            self.performSegue(withIdentifier: "enterQuestionSegue", sender: self)
        }
        
        if question.active {
            let archive = UITableViewRowAction(style: .destructive, title: "Archive") { (action, indexPath) in
                let question = self.items[indexPath.row]
                question.active = false
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
                dynamoDBObjectMapper.save(question).continueWith { (task) -> Any? in
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        if let error = task.error as NSError? {
                            print("The request failed. Error: \(error)")
                        } else {
                            self.getQuestions()
                        }
                    }
                }
            }
            return [archive, edit]
        } else if !question.active {
            let unarchive = UITableViewRowAction(style: .destructive, title: "Unarchive") { (action, indexPath) in
                let question = self.items[indexPath.row]
                question.active = true
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
                dynamoDBObjectMapper.save(question).continueWith { (task) -> Any? in
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        if let error = task.error as NSError? {
                            print("The request failed. Error: \(error)")
                        }
                        else {
                            self.getQuestions()
                        }
                    }
                }
            }
            unarchive.backgroundColor = UIColor.brown
            return [unarchive, edit]
        }
        return [edit]
    }
}

extension AskedQuestionsViewController: UITableViewDataSource { // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.items.count == 0 {
            let message = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
            message.text = "No questions found"
            message.textAlignment = NSTextAlignment.center
            message.sizeToFit()
            //set tableview background
            self.tableView.backgroundView = message
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AskedQuestionCell", for: indexPath) as! AskedQuestionCell
        cell.qtnTextLabel?.text = items[indexPath.row].name
        let date = DateInRegion(NSDate(timeIntervalSince1970: Double(items[indexPath.row].timeAdded!)!) as Date, region: Region.currentIn())
        //let (colloquial, _) = try! date.colloquialSinceNow()
        cell.footnoteLabel?.text = "Added: \(date.toRelative())"
        let newAnswer = items[indexPath.row].isNewAnswer
        cell.qtnStatusBadge.isHidden = newAnswer ? false : true
        cell.qtnTextLabel.font = newAnswer ? UIFont.boldSystemFont(ofSize: 17.0) : UIFont.preferredFont(forTextStyle: .callout)
        cell.footnoteLabel.font = newAnswer ? UIFont.boldSystemFont(ofSize: 14.0) : UIFont.preferredFont(forTextStyle: .footnote)
        return cell
    }
}

extension AskedQuestionsViewController: QuestionSavedDelegate {
    func onQuestionSaved() {
        getQuestions()
    }
}
