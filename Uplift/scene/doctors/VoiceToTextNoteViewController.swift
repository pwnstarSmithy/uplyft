//
//  VoiceToTextNoteViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 02/05/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import Speech
import Firebase
import PKHUD

class VoiceToTextNoteViewController: UIViewController {
    
    @IBOutlet weak var microphoneButton: UIButton!
    @IBOutlet weak var recordedTextView: UILabel!
    @IBOutlet weak var micInstructionsLabel: UILabel!
    
    var noteRef = DatabaseReference()
    var patientEmail = ""
    var doctorEmail  = ""
    var saveNoteDelegate: SaveNotesDelegate?
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    enum SpeechStatus {
        case ready
        case recognizing
        case unavailable
    }
    
    var status = SpeechStatus.ready {
        didSet {
            self.setUI(status: status)
        }
    }
    
    @IBAction func microphonePressed(_ sender: Any) {
        switch status {
        case .ready:
            startRecording()
            status = .recognizing
        case .recognizing:
            cancelRecording()
            status = .ready
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        switch SFSpeechRecognizer.authorizationStatus() {
        case .notDetermined:
            askSpeechPermission()
        case .authorized:
            self.status = .ready
        case .denied, .restricted:
            self.status = .unavailable
        }
        
        noteRef = Database.database().reference(withPath: "notes")
        noteRef.keepSynced(true)
        self.hideKeyboardWhenTappedAround()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.shadowImage = #imageLiteral(resourceName: "default_color_pixel")
        navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "default_color_pixel"), for: .default)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
    }
    
    func addDoneButton() {
        //let rightBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(saveNote))
        let rightBarButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveNote))
        let _ = UIBarButtonItem(barButtonSystemItem: .undo, target: self, action: #selector(clearLabelText))
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func removeDoneButton() {
        guard (recordedTextView.text == nil), (recordedTextView.text?.isEmpty)! else {
            return
        }
        navigationItem.rightBarButtonItem = nil
    }
    
    @objc func saveNote() {
        guard (recordedTextView.text != nil), !(recordedTextView.text?.isEmpty)! else {
            return
        }
        HUD.show(.progress)
        Shared.saveNote(text: recordedTextView.text!, doctorEmail: doctorEmail, patientEmail: patientEmail) { (error, ref) in
            DispatchQueue.main.async {
                HUD.hide()
                if error == nil {
                    self.saveNoteDelegate?.onNoteSaved()
                    self.navigationController?.popViewController(animated: true)
                } else {
                    Shared.showAlert(title: "New Note Error", message: (error?.localizedDescription)!, viewController: self)
                }
            }
        }
    }
    
    @objc func clearLabelText() {
        let text = self.recordedTextView.text ?? ""
        self.recordedTextView.text = String(text.dropLast())
    }
    
    func setUI(status: SpeechStatus) {
        switch status {
        case .ready:
            microphoneButton.setImage(#imageLiteral(resourceName: "available"), for: .normal)
            micInstructionsLabel.text = "Tap button to start"
            removeDoneButton()
        case .recognizing:
            microphoneButton.setImage(#imageLiteral(resourceName: "stop"), for: .normal)
            micInstructionsLabel.text = "Listening..."
            addDoneButton()
        case .unavailable:
            microphoneButton.setImage(#imageLiteral(resourceName: "unavailable"), for: .normal)
            micInstructionsLabel.text = ""
            removeDoneButton()
        }
    }
    
    func askSpeechPermission() {
        SFSpeechRecognizer.requestAuthorization { status in
            OperationQueue.main.addOperation {
                switch status {
                case .authorized:
                    self.status = .ready
                default:
                    self.status = .unavailable
                }
            }
        }
    }
    
    func startRecording() {
        // Setup audio engine and speech recognizer
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        
        // Prepare and start recording
        audioEngine.prepare()
        do {
            try audioEngine.start()
            self.status = .recognizing
        } catch {
            return print(error)
        }
        
        // Analyze the speech
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {
                self.recordedTextView.text = result.bestTranscription.formattedString
            } else if let error = error {
                print(error)
            }
        })
    }
    
    func cancelRecording() {
        audioEngine.stop()
        let node = audioEngine.inputNode
        node.removeTap(onBus: 0)
        
        recognitionTask?.cancel()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
