//
//  PatientsViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 13/10/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider
import Firebase
import PKHUD

var allPatients: [AWSCognitoIdentityProviderUserType] = []

class PatientsViewController: UIViewController {
    
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var items: [Patient] = []
    var patients: [AWSCognitoIdentityProviderUserType] = []
    var selectedSegmentControlIndex = 0
    
    let prefs:UserDefaults = UserDefaults.standard
    var patientRef = DatabaseReference()
    var currentUser: ULUser!
    
    var selectedCellIndexPath: IndexPath!
    
    var awsCognitoIdentityProvider:AWSCognitoIdentityProvider?
    //var doctorAttributes:AWSCognitoIdentityUserGetDetailsResponse?
    
    @IBAction func segmentValueChanged(_ sender: Any) {
        switch segmentControl.selectedSegmentIndex {
        case 1:
            selectedSegmentControlIndex = 1
            reloadTableView(enabled: 0)
        default:
            selectedSegmentControlIndex = 0
            reloadTableView(enabled: 1)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USWest2,identityPoolId:"us-west-2:7fcee860-434a-4d93-8357-0231609ad65c")
        let configuration = AWSServiceConfiguration(region:.USWest2, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        awsCognitoIdentityProvider = AWSCognitoIdentityProvider.default()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Reload")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        getActiveDoctorPatients()
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        getActiveDoctorPatients()
    }
    
    func reloadTableView(enabled: NSNumber) {
        patients = allPatients
        patients = patients.filter { $0.enabled == enabled }
        tableView.reloadData()
        if self.patients.count > 0 {
            self.tableView.backgroundView = nil
        } else {
            let background = Background()
            background.delegate = self
            self.tableView.backgroundView = background
        }
    }
    
    func getActiveDoctorPatients() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let getUsersRequest = AWSCognitoIdentityProviderListUsersRequest()
        // Add the Parameters
        getUsersRequest?.userPoolId = awsPoolID
        getUsersRequest?.filter = "family_name = \"\(AppDelegate.defaultUserPool().currentUser()!.username!)\""
        getUsersRequest?.limit = 0
        awsCognitoIdentityProvider?.listUsers(getUsersRequest!, completionHandler: { (response, error) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.refreshControl.endRefreshing()
                if let error = error as NSError? {
                    print(error)
                } else {
                    if let res = response {
                        print(res.users ?? "No users were found for doctor")
                        if let users = res.users {
                            allPatients = users.reversed()
                            if self.selectedSegmentControlIndex == 1 {
                                self.reloadTableView(enabled: 0)
                            } else {
                                self.reloadTableView(enabled: 1)
                            }
                        }
                    }
                }
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.shadowImage = #imageLiteral(resourceName: "default_color_pixel")
        navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "default_color_pixel"), for: .default)
        if patients.count == 0 {
            let background = Background()
            background.delegate = self
            self.tableView.backgroundView = background
        } else {
            self.tableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func addPatientNavBarButtonTapped(_ sender: Any) {
        addPatient()
    }
    
    func addPatient() {
        let alert = UIAlertController(title: "Add Patient",
                                      message: "",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) { action in
            let nameField = alert.textFields![0]
            let emailField = alert.textFields![1]
            
            guard let nameText = nameField.text, let emailText = emailField.text else { return }
            
            if !nameText.isEmpty, !emailText.isEmpty {
                
                if Shared.isValidEmail(testStr: emailText) {
                    /// First add the patient as a firebase user
                    HUD.show(.progress)
                    let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USWest2,identityPoolId:"us-west-2:7fcee860-434a-4d93-8357-0231609ad65c")
                    let configuration = AWSServiceConfiguration(region:.USWest2, credentialsProvider:credentialsProvider)
                    
                    AWSServiceManager.default().defaultServiceConfiguration = configuration
                    let nsdateAdded = NSDate().timeIntervalSince1970
                    let allowAnswersEdit = UserDefaults.standard.bool(forKey: PatientsSettings.allowPatientsToEditAnswers.rawValue)
                    let attributes:[AWSCognitoIdentityUserAttributeType] = [
                        AWSCognitoIdentityUserAttributeType(name: "custom:is_patient", value: "1"),
                        AWSCognitoIdentityUserAttributeType(name: "custom:add_by_doctor", value: AppDelegate.defaultUserPool().currentUser()!.username ?? ""),
                        AWSCognitoIdentityUserAttributeType(name: "family_name", value: AppDelegate.defaultUserPool().currentUser()!.username ?? ""),
                        AWSCognitoIdentityUserAttributeType(name: "given_name", value: nameText),
                        AWSCognitoIdentityUserAttributeType(name: "email", value: emailText),
                        AWSCognitoIdentityUserAttributeType(name: "email_verified", value: "true"),
                        AWSCognitoIdentityUserAttributeType(name: "custom:active", value: "1"),
                        AWSCognitoIdentityUserAttributeType(name: "custom:timeAdded", value: "\(nsdateAdded)"),
                        AWSCognitoIdentityUserAttributeType(name: "custom:allowAnswersEdit", value: "\(allowAnswersEdit ? 1 : 0 )"),
                    ]
                    let character = emailText as NSString
                    let params = [
                        "UserPoolId": awsPoolID,
                        "Username": emailText,
                        "DesiredDeliveryMediums": ["EMAIL"],
                        "ForceAliasCreation": false,
                        "MessageAction": "SUPPRESS",
                        "TemporaryPassword": "LYXPATIenT\(character.character(at: 0))",
                        "UserAttributes": attributes
                    ] as [String : Any]
                    
                    do {
                        let req: AWSCognitoIdentityProviderAdminCreateUserRequest = try AWSCognitoIdentityProviderAdminCreateUserRequest(dictionary: params, error: ())
                        self.awsCognitoIdentityProvider?.adminCreateUser(req) { (response, error) in
                            DispatchQueue.main.async {
                                HUD.hide()
                                if let error = error as NSError? {
                                    Shared.showAlert(title: "Add Patient", message: error.userInfo["message"] as? String ?? error.localizedDescription, viewController: self)
                                } else {
                                    if let res = response {
                                        if let user = res.user {
                                            print(res.user ?? "User data not returned")
                                            self.patients.insert(user, at: 0)
                                            self.tableView.backgroundView = nil
                                            self.tableView.reloadData()
                                        }
                                    }
                                }
                            }
                        }
                    } catch let error {
                        print(error)
                    }
                } else {
                    Shared.showAlert(title: "Add Patient", message: "An invald email was entered. Please try again", viewController: self)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        alert.addTextField { textEmail in
            textEmail.placeholder = "Enter patient name"
            textEmail.heightAnchor.constraint(equalToConstant: 25).isActive = true
        }
        
        alert.addTextField { textEmail in
            textEmail.placeholder = "Enter patient email"
            textEmail.heightAnchor.constraint(equalToConstant: 25).isActive = true
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func removeUnreadFlag(indexPath: IndexPath) {
        let patient = items[indexPath.row]
        // here use sanitized email since firebase rejects some characters from being used as part of the keys in the database
        var sanitizedEmail = patient.email
        sanitizedEmail.removingRegexMatches(pattern: "[\\[\\]\\./#$]+")
        patient.ref?.child("newAnswersAdded/\(sanitizedEmail)").removeValue(completionBlock: { (error, reference) in
            if let err = error {
                print(err.localizedDescription)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "askedQuestionsSegue" {
            if let selectedCellIndexPath = selectedCellIndexPath {
                let askedQuestionsController = segue.destination as! AskedQuestionsViewController
                let name = patients[selectedCellIndexPath.row].attributes!.filter { $0.name == "given_name"}
                askedQuestionsController.patientName = (name.first?.value)!
                let email = patients[selectedCellIndexPath.row].attributes!.filter { $0.name == "email"}
                askedQuestionsController.patientEmail = (email.first?.value)!
                askedQuestionsController.patientUuid = patients[selectedCellIndexPath.row].username!
                askedQuestionsController.doctorEmail = AppDelegate.defaultUserPool().currentUser()!.username!
                let defaults = UserDefaults.standard
                askedQuestionsController.doctorName = defaults.string(forKey: "NAME") ?? ""
            }
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if selectedCellIndexPath == nil {
            return false
        }
        return true
    }
}

extension PatientsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientCell", for: indexPath) as! PatientListCell
        let patient = patients[indexPath.row].attributes
        let name = patient?.filter { $0.name == "given_name" }
        cell.nameLabel?.text = name?.first?.value
        let email = patient?.filter { $0.name == "email" }
        cell.emailLabel?.text = email?.first?.value
        // here use a sanitized email with eliminated chars since firebase rejects some characters from being used as part of the keys
        /*var sanitizedDocEmail = patient.email
        sanitizedDocEmail.removingRegexMatches(pattern: "[\\[\\]\\./#$]+")
        let newAnswer = patient.newAnswersAdded.keys.contains(sanitizedDocEmail)*/
        cell.newQuestionStatusBadge.isHidden = true
        //cell.nameLabel.font = newAnswer ? UIFont.boldSystemFont(ofSize: 17.0) : UIFont.preferredFont(forTextStyle: .callout)
        //cell.emailLabel.font = newAnswer ? UIFont.boldSystemFont(ofSize: 14.0) : UIFont.preferredFont(forTextStyle: .footnote)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let patient = patients[indexPath.row]
        if patient.enabled! == 1 {
            let archive = UITableViewRowAction(style: .destructive, title: "Archive") { (action, indexPath) in
                let req = AWSCognitoIdentityProviderAdminDisableUserRequest()
                req?.username = patient.username!
                req?.userPoolId = awsPoolID
                self.awsCognitoIdentityProvider?.adminDisableUser(req!, completionHandler: { (response, error) in
                    DispatchQueue.main.async {
                        if error != nil {
                            Shared.showAlert(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                        } else {
                            self.getActiveDoctorPatients()
                        }
                    }
                })
            }
            return [archive]
        } else {
            let unarchive = UITableViewRowAction(style: .normal, title: "Unarchive") { (action, indexPath) in
                let req = AWSCognitoIdentityProviderAdminEnableUserRequest()
                req?.username = patient.username!
                req?.userPoolId = awsPoolID
                self.awsCognitoIdentityProvider?.adminEnableUser(req!, completionHandler: { (response, error) in
                    DispatchQueue.main.async {
                        if error != nil {
                            Shared.showAlert(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                        } else {
                            self.getActiveDoctorPatients()
                        }
                    }
                    
                })
            }
            unarchive.backgroundColor = UIColor.darkGray
            return [unarchive]
        }
    }
}

extension PatientsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCellIndexPath = indexPath
        tableView.deselectRow(at: indexPath, animated: true)
        //removeUnreadFlag(indexPath: indexPath)
        performSegue(withIdentifier: "askedQuestionsSegue", sender: self)
    }
}

extension PatientsViewController: BackgroundAddPatientsProtocol {
    func addPatientsButtonTapped() {
        addPatient()
    }
}

extension UITextField {
    func textbottommboarder(frame1 : CGRect){
        let border = UIView()
        let width = CGFloat(2.0)
        border.backgroundColor = UIColor.lightGray
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  frame1.width, height: 2)
        self.layer.addSublayer(border.layer)
        self.layer.masksToBounds = true
    }
}

