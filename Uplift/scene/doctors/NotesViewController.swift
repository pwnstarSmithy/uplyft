//
//  NotesViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 01/05/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSDynamoDB
import SwiftDate

protocol SaveNotesDelegate {
    func onNoteSaved()
}

class NotesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var items = [Note]()
    var addByDoctor = ""
    var forPatient = ""
    var selectedNote: Note?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Notes"
        addNoteButton()
        getPatientQuestions()
    }
    
    func getPatientQuestions() {
        let scanExpression = AWSDynamoDBScanExpression()
        scanExpression.filterExpression = "active = :val AND addedByDoctor = :doc AND forPatient = :patient"
        scanExpression.expressionAttributeValues = [":val": true, ":doc" : addByDoctor, ":patient" : forPatient]
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
        dynamoDBObjectMapper.scan(Note.self, expression: scanExpression).continueWith { (task) -> Any? in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let error = task.error as NSError? {
                    print("The request failed. Error: \(error)")
                } else if let paginatedOutput = task.result {
                    self.items =  paginatedOutput.items as! [Note]
                    self.items = self.items.sorted(by: { (note1, note2) -> Bool in
                        Double(note1.timeAdded!)! > Double(note2.timeAdded!)!
                    })
                    dump(self.items)
                    if self.items.count > 0 {
                        self.tableView.backgroundView = nil
                        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func addNoteButton() {
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNote))
        navigationItem.rightBarButtonItem = rightBarButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func addNote() {
        let alertController = UIAlertController(title: "Add note category", message: nil, preferredStyle: .actionSheet)
        let typeNote = UIAlertAction(title: "Type note text", style: .default, handler: { (action) -> Void in
            self.performSegue(withIdentifier: "typeNoteSegue", sender: self)
        })
        let voiceToTextNote = UIAlertAction(title: "Voice to text note", style: .default, handler: { (action) -> Void in
            
            let alertController = UIAlertController(title: "Please Note", message: "This feature uses Siri to analize voice and has a limitaion of about 60 seconds of anlaysis for each speech recognition task. This also has a high impact on network traffic and power usage hence may have an impact on your device battery & connection.", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Ok button a alert"), style: .default) { (action) in
                print("Take user to voice to text")
                self.performSegue(withIdentifier: "voiceToTextNoteSegue", sender: self)
            }
            let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(OKAction)
            alertController.addAction(cancelButton)
            self.present(alertController, animated: true, completion: nil)
            
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(typeNote)
        alertController.addAction(voiceToTextNote)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "typeNoteSegue" {
            let typeNoteVC = segue.destination as! TypeNoteViewController
            typeNoteVC.doctorEmail = self.addByDoctor
            typeNoteVC.patientEmail = self.forPatient
            typeNoteVC.saveNoteDelegate = self
            typeNoteVC.noteToEdit = selectedNote
        } else if segue.identifier == "voiceToTextNoteSegue" {
            let voiceToTextVC = segue.destination as! VoiceToTextNoteViewController
            voiceToTextVC.doctorEmail = self.addByDoctor
            voiceToTextVC.patientEmail = self.forPatient
            voiceToTextVC.saveNoteDelegate = self
        }
    }
 

}

extension NotesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            print("Edit action received at \(indexPath.row)")
            self.selectedNote = self.items[indexPath.row]
            self.performSegue(withIdentifier: "typeNoteSegue", sender: self)
        }
        return [edit]
    }
}

extension NotesViewController: UITableViewDataSource { // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.items.count == 0 {
            let message = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
            message.text = "Notes not found"
            message.textAlignment = NSTextAlignment.center
            message.sizeToFit()
            //set tableview background
            self.tableView.backgroundView = message
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotesCell", for: indexPath) as! NotesCell
        cell.noteText.text = items[indexPath.row].text
        let date = DateInRegion(NSDate(timeIntervalSince1970: Double(items[indexPath.row].timeAdded!)!) as Date, region: Region.currentIn())
        //let (colloquial, _) = try! date.colloquialSinceNow()
        cell.dateAddedLabel.text = "Added: \(date.toRelative())"
        return cell
    }
}

extension NotesViewController: SaveNotesDelegate {
    func onNoteSaved() {
        getPatientQuestions()
    }
}
