//
//  SelectQuestionCell.swift
//  Uplift
//
//  Created by Harold Asiimwe on 01/12/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import UIKit

class SelectQuestionCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
