//
//  NotesCell.swift
//  Uplift
//
//  Created by Harold Asiimwe on 01/05/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit

class NotesCell: UITableViewCell {

    @IBOutlet weak var noteText: UILabel!
    @IBOutlet weak var dateAddedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
