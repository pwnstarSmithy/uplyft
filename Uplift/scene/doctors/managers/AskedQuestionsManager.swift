//
//  AskedQuestionsManager.swift
//  Uplift
//
//  Created by Harold Asiimwe on 21/11/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import Foundation
import Firebase
import AWSDynamoDB

enum QuestionCategory: String {
    case thoughtRecord, avoidanceSufferingDiary
}

class AskedQuestionsManager {
    
    struct QuestionStore {
        
        var doctorEmail = ""
        var doctorName = ""
        var patientEmail = ""
        var questionRef = DatabaseReference()
        
        let questions: [(name:String, value: String)] = [
            (name: "Situation / Trigger", value: "What happened? Where? When? Who with? How?"),
            (name:"Feelings Emotions – (Rate 0 – 100%) Body sensations", value: "What emotion did I feel at that time? What else? How intense was it? What did I notice in my body? Where did I feel it?"),
            (name: "Unhelpful Thoughts / Images", value:
            "What went through my mind? What disturbed me? What did those thoughts/images/memories mean to me, or say about me or the situation? What am I responding to? What ‘button’ is this pressing for me? What would be the worst thing about that, or that could happen?"),
            (name: "Facts that support the unhelpful thought", value: "What are the facts? What facts do I have that the unhelpful thought/s are totally true?"),
            (name: "Facts that provide evidence against the unhelpful thought", value:
            "What facts do I have that the unhelpful thought/s are NOT totally true? Is it possible that this is opinion, rather than fact? What have others said about this?"),
            (name: "Alternative, more realistic and balanced perspective", value:
            "STOPP! Take a breath…. What would someone else say about this situation? What’s the bigger picture? Is there another way of seeing it? What advice would I give someone else? Is my reaction in proportion to the actual event? Is this really as important as it seems?"),
            (name: "Outcome Re-rate emotion", value: "What am I feeling now? (0-100%) What could I do differently? What would be more effective? Do what works! Act wisely. What will be most helpful for me or the situation? What will the consequences be?")
        ]
        
        let avoidanceSufferingQuestions: [(name: String, value: String)] = [
            (name: "Painful Thoughts/ Feelings/ Sensations/ Memories that showed up today", value: "Painful Thoughts/ Feelings/ Sensations/ Memories that showed up today"),
            (name: "What I did to escape, avoid, get rid of them, or distract myself from them", value: "What I did to escape, avoid, get rid of them, or distract myself from them"),
            (name: "What that cost me in terms of health, vitality, relationship issues, getting stuck, increasing pain, wasted time/money/energy etc.",
             value:"What that cost me in terms of health, vitality, relationship issues, getting stuck, increasing pain, wasted time/money/energy etc.")
        ]
        
        func addQuestions(questionCategory: QuestionCategory) {
            switch questionCategory {
            case .thoughtRecord:
                //Add thought record questions
                saveQuestions(questions: questions, onSave: { error, response in
                    
                })
            default:
                saveQuestions(questions: avoidanceSufferingQuestions, onSave: { error, response in
                    
                })
            }
        }
        
        func addCategoryQuestions(questions:[(name:String, value:String)], onSave callback: @escaping (Error?, AnyObject?)->Void) {
            saveQuestions(questions: questions, onSave: callback)
        }
        
        private func saveQuestions(questions: [(name: String, value: String)], onSave callback: @escaping (Error?, AnyObject?)->Void) {
            var writeRequests: [AWSDynamoDBWriteRequest] = []
            for question in questions {
                let nsdateAdded = NSDate().timeIntervalSince1970
                
                let uuid = AWSDynamoDBAttributeValue()
                uuid?.s = NSUUID().uuidString.lowercased()
                
                let name = AWSDynamoDBAttributeValue()
                name?.s = question.name
                
                let addedByDoctor = AWSDynamoDBAttributeValue()
                addedByDoctor?.s = doctorEmail
                
                let docName = AWSDynamoDBAttributeValue()
                docName?.s = doctorName.isEmpty ? doctorEmail : doctorName
                
                let questionText = AWSDynamoDBAttributeValue()
                questionText?.s = question.value
                
                let belongsTo = AWSDynamoDBAttributeValue()
                belongsTo?.s = patientEmail
                
                let timeAdded = AWSDynamoDBAttributeValue()
                timeAdded?.s = "\(nsdateAdded)"
                
                let isActive = AWSDynamoDBAttributeValue()
                isActive?.boolean = true
                
                let isNewAnswer = AWSDynamoDBAttributeValue()
                isNewAnswer?.boolean = false
                
                let questionParams = [
                    "uuid": uuid!,
                    "name": name!,
                    "questionText" : questionText!,
                    "addedByDoctor": addedByDoctor!,
                    "doctorName": docName!,
                    "belongsTo": belongsTo!,
                    "active": isActive!,
                    "timeAdded": timeAdded!,
                    "isNewAnswer": isNewAnswer!
                ]
                
                let writeRequest = AWSDynamoDBWriteRequest()
                writeRequest?.putRequest = AWSDynamoDBPutRequest()
                writeRequest?.putRequest?.item = questionParams
                writeRequests.append(writeRequest!)
            }
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            let batchWriteItemInput = AWSDynamoDBBatchWriteItemInput()
            batchWriteItemInput?.requestItems = ["Questions": writeRequests]
            let dynamoDB = AWSDynamoDB.default()
            dynamoDB.batchWriteItem(batchWriteItemInput!) { (aWSDynamoDBBatchWriteItemOutput, error) in
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    callback(error, aWSDynamoDBBatchWriteItemOutput)
                    if let error = error {
                        print("The request failed. Error: \(error)")
                    }
                }
            }
        }
    }
    
    func questionAlreadyAsked(questionCatergory: QuestionCategory, questions:[Question]) -> Bool {
        switch questionCatergory {
        case .thoughtRecord:
            let thoughtRecord = QuestionStore()
            for question in questions {
                for qtn in thoughtRecord.questions {
                    if question.name == qtn.name && question.questionText == qtn.value {
                        return true
                    }
                }
            }
        default:
            let avoidanceSufferingQtns = QuestionStore()
            for question in questions {
                for qtn in avoidanceSufferingQtns.avoidanceSufferingQuestions {
                    if question.name == qtn.name && question.questionText == qtn.value {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    func getUnSelectedQuestions(questionCatergory: QuestionCategory, questions:[Question]) -> [(name: String, value:String)] {
        switch questionCatergory {
            case .thoughtRecord:
                let thoughtRecord = QuestionStore()
                var thoughtRecordQuestions = thoughtRecord.questions
                for question in questions {
                    for qtn in thoughtRecordQuestions {
                        if question.name == qtn.name {
                            let index = thoughtRecordQuestions.index(where: { (thoughtRecQtn) -> Bool in
                                thoughtRecQtn.name == qtn.name
                            })
                            thoughtRecordQuestions.remove(at: index!)
                        }
                    }
                }
                return thoughtRecordQuestions
            case .avoidanceSufferingDiary:
                let avoidance = QuestionStore()
                var avoidanceQuestions = avoidance.avoidanceSufferingQuestions
                for question in questions {
                    for qtn in avoidanceQuestions {
                        if qtn.name == question.name {
                            let index = avoidanceQuestions.index(where: { (thoughtRecQtn) -> Bool in
                                thoughtRecQtn.name == qtn.name
                            })
                            avoidanceQuestions.remove(at: index!)
                        }
                    }
                }
                return avoidanceQuestions
        }
    }
}
