//
//  EnterAnswerViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 07/11/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import UIKit
import GrowingTextView
import AWSCognitoIdentityProvider
import AWSDynamoDB
import PKHUD

class EnterAnswerViewController: UIViewController, GrowingTextViewDelegate {
    
    var awsCognitoIdentityProvider:AWSCognitoIdentityProvider?
    var question: Question!
    var answerToBeEdited: Answer!
    var patientAttributes:AWSCognitoIdentityUserGetDetailsResponse?
    var saveAnswerDelegate: SaveAnswerDelegate?
    
    @IBOutlet weak var enterAnswerTextView: GrowingTextView!
    
    
    @IBAction func saveAnswerButtonTapped(_ sender: Any) {
        if enterAnswerTextView.isFirstResponder {
            enterAnswerTextView.resignFirstResponder()
        }
        if enterAnswerTextView.text.isEmpty {
            Shared.showAlert(title: "Enter Answer", message: "No answer text was entered", viewController: self)
            return
        }
        
        if answerToBeEdited != nil {
            saveAnswerEdit(text: enterAnswerTextView.text)
        } else {
            saveAnswer(text: enterAnswerTextView.text)
            saveAnswerDelegate?.onAnswerSaved()
            navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USWest2,identityPoolId:"us-west-2:7fcee860-434a-4d93-8357-0231609ad65c")
        let configuration = AWSServiceConfiguration(region:.USWest2, credentialsProvider:credentialsProvider)

        AWSServiceManager.default().defaultServiceConfiguration = configuration
        awsCognitoIdentityProvider = AWSCognitoIdentityProvider.default()
        self.hideKeyboardWhenTappedAround()
        
        if answerToBeEdited != nil {
            enterAnswerTextView.text = answerToBeEdited.name
        }
        
        AppDelegate.defaultUserPool().currentUser()?.getDetails().continueWith { (task) -> Any? in
            dump(task)
            if task.error != nil {
            } else {
                self.patientAttributes = task.result
                dump(self.patientAttributes?.userAttributes)
            }
            return nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.view.endEditing(true)
    }
    
    func saveAnswer(text: String) {
        HUD.show(.progress)
        Shared.saveAnswer(text: text, questionKey: question.uuid!) { (_ questionKey) in
            DispatchQueue.main.async {
                HUD.hide()
                if questionKey.isEmpty {
                    Shared.showAlert(title: "New Answer Error", message: "Unable to save answer, try again", viewController: self)
                } else {
                    
                    print("Answer was successfully saved")
                }
            }
        }
        
        if let pat = patientAttributes {
            setNotification(patient: pat)
            updatePatientAndQuestionNewAnswerFlags()
        }
    }
    
    func saveAnswerEdit(text:String) {
        HUD.show(.progress)
        if let answer = answerToBeEdited {
            answer.name = text
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
            dynamoDBObjectMapper.save(answer).continueWith { (task) -> Any? in
                DispatchQueue.main.async {
                    HUD.hide()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if let error = task.error as NSError? {
                        print("The request failed. Error: \(error)")
                        Shared.showAlert(title: "Error", message: (error.localizedDescription), viewController: self)
                    } else {
                        self.saveAnswerDelegate?.onAnswerSaved()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
        
        if let pat = patientAttributes {
            setNotification(patient: pat)
            updatePatientAndQuestionNewAnswerFlags()
        }
    }
    
    func setNotification(patient: AWSCognitoIdentityUserGetDetailsResponse) {
        let tokens = patient.userAttributes?.filter { $0.name == "custom:deviceTokens" }
        let tokenValues = tokens?.first?.value
        let name = patient.userAttributes?.filter { $0.name == "name" }
        let notData: [String: Any] = [
            "to" : tokenValues?.components(separatedBy: ",").last ?? "",
            "notification": [
                "title" : "New Answer Received",
                "body"  : "✨ A new answer has been submited by your patient \(name?.first?.value ?? "") ✍️"
            ] as Dictionary
        ]
        Shared.sendPushNotification(notificationData: notData)
    }
    
    func updatePatientAndQuestionNewAnswerFlags() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let req = AWSCognitoIdentityProviderAdminUpdateUserAttributesRequest()!
        req.username = AppDelegate.defaultUserPool().currentUser()?.username!
        req.userPoolId = awsPoolID
        req.userAttributes = [AWSCognitoIdentityUserAttributeType(name: "custom:newAnswersAdded", value: "1")]
        awsCognitoIdentityProvider?.adminUpdateUserAttributes(req, completionHandler: { (response, error) in
            if error == nil {
                self.question.isNewAnswer = true
                let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
                dynamoDBObjectMapper.save(self.question).continueWith { (task) -> Any? in
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        if let error = task.error as NSError? {
                            print("The request failed. Error: \(error)")
                        }
                    }
                }
            } else {
                print("The request failed. Error: \(String(describing: error))")
            }
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
