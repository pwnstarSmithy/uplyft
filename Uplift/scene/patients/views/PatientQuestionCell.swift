//
//  PatientQuestionCell.swift
//  Uplift
//
//  Created by Harold Asiimwe on 30/03/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit

class PatientQuestionCell: UITableViewCell {

    @IBOutlet weak var footnoteLabel: UILabel!
    @IBOutlet weak var questionTextLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
