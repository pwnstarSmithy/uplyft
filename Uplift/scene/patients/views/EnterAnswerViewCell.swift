//
//  EnterAnswerViewCell.swift
//  Uplift
//
//  Created by Harold Asiimwe on 03/01/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import GrowingTextView

class EnterAnswerViewCell: UITableViewCell {
    
    @IBOutlet weak var enterAnswerTextView: GrowingTextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        enterAnswerTextView.minHeight = contentView.bounds.height
        enterAnswerTextView.maxHeight = contentView.bounds.height
        let heightContraints = NSLayoutConstraint(item: enterAnswerTextView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: contentView.bounds.height)
        NSLayoutConstraint.activate([heightContraints])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension EnterAnswerViewCell: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.contentView.layoutIfNeeded()
        }
    }
}
