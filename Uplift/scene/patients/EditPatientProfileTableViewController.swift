//
//  EditPatientProfileTableViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 03/03/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider
import PKHUD

class EditPatientProfileTableViewController: UITableViewController {
    
    @IBOutlet weak var changePasswordCell: UITableViewCell!
    
    @IBOutlet weak var changeNameCell: UITableViewCell!
    
    @IBOutlet weak var addPhoneCell: UITableViewCell!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    @IBOutlet weak var emailCell: UITableViewCell!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    var patient: Patient?
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.navigationItem.title = "Edit Profile"
        self.setPatientPhone()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatientPhone() {
        phoneNumberLabel.text = defaults.string(forKey: "PHONE") ?? ""
    }
    
    func changeUserPassword() {
        showEnterCurrentPasswordAlert()
    }
    
    func showEnterCurrentPasswordAlert() {
        let alert = UIAlertController(title: "Current Password",
                                      message: "",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Submit", style: .default) { action in
            let passwordField = alert.textFields![0]
            guard let passwordText = passwordField.text else { return }
            if !passwordText.isEmpty {
                self.showEnterNewPasswordAlert(currentPassword: passwordText)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        alert.addTextField { textPassword in
            textPassword.isSecureTextEntry = true
            textPassword.placeholder = "Enter current password"
            textPassword.heightAnchor.constraint(equalToConstant: 25).isActive = true
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showEnterNewPasswordAlert(currentPassword: String) {
        let alert = UIAlertController(title: "New Password",
                                      message: "",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) { action in
            let passwordField = alert.textFields![0]
            let confirmPasswordField = alert.textFields![1]
            guard let passwordText = passwordField.text, let confirmPasswordText = confirmPasswordField.text else { return }
            if !passwordText.isEmpty, !confirmPasswordText.isEmpty {
                if passwordText != confirmPasswordText {
                    Shared.showAlert(title: "Change password", message: "Passwords do not match.\n Please try again.", viewController: self)
                    return
                }
                if passwordText.count < 8 {
                    Shared.showAlert(title: "Change Password Error", message: "Passwords must have a minimum of 8 characters", viewController: self)
                    return
                }
                
                if !Shared.isPasswordValid(text: passwordText) {
                    Shared.showAlert(title: "Change Password Error",
                                     message: "Passwords must contain a combination of upper case, lower case and number characters", viewController: self)
                    return
                }
                
                HUD.show(.progress)
                AppDelegate.defaultUserPool().currentUser()?.changePassword(currentPassword, proposedPassword: passwordText).continueOnSuccessWith(block: { (task) -> Any? in
                    DispatchQueue.main.async {
                        if task.error == nil {
                            HUD.flash(.success, delay: 2.0)
                        } else {
                            Shared.showAlert(title: "Change password Error", message: (task.error?.localizedDescription)!, viewController: self)
                        }
                    }
                })
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        alert.addTextField { textPassword in
            textPassword.isSecureTextEntry = true
            textPassword.placeholder = "Enter new password"
            textPassword.heightAnchor.constraint(equalToConstant: 25).isActive = true
        }
        
        alert.addTextField { textPassword in
            textPassword.isSecureTextEntry = true
            textPassword.placeholder = "Re enter password"
            textPassword.heightAnchor.constraint(equalToConstant: 25).isActive = true
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showChangeDisplayNameAlert() {
        let alert = UIAlertController(title: "New name",
                                      message: "",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) { action in
            let nameField = alert.textFields![0]
            guard let nameText = nameField.text else { return }
            if !nameText.isEmpty {
                HUD.show(.progress)
                let nameAttribute = AWSCognitoIdentityUserAttributeType()
                nameAttribute?.name = "given_name"
                nameAttribute?.value = nameText
                AppDelegate.defaultUserPool().currentUser()?.update([nameAttribute!]).continueOnSuccessWith(block: { (task) -> Any? in
                    DispatchQueue.main.async {
                        if task.error == nil {
                            dump(task.result)
                            AppDelegate.defaultUserPool().currentUser()?.getDetails().continueOnSuccessWith(block: { (task) -> Any? in
                                guard task.result != nil else {
                                    Shared.showAlert(title: "Change name Error", message: (task.error?.localizedDescription)!, viewController: self)
                                    return nil
                                }
                                var userAttributes:[AWSCognitoIdentityProviderAttributeType]?
                                userAttributes = task.result?.userAttributes
                                let nameValue = userAttributes?.filter { $0.name == "given_name" }
                                let defaults = UserDefaults.standard
                                defaults.set(nameValue?.first?.value ?? "", forKey: "NAME")
                                defaults.synchronize()
                                DispatchQueue.main.async {
                                    HUD.hide()
                                    self.userNameLabel.text = nameText
                                    self.tableView.reloadData()
                                }
                                return nil
                            })
                        } else {
                            HUD.hide()
                            Shared.showAlert(title: "Change name Error", message: (task.error?.localizedDescription)!, viewController: self)
                        }
                    }
                })
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        alert.addTextField { textPassword in
            textPassword.placeholder = "Enter name"
            textPassword.heightAnchor.constraint(equalToConstant: 25).isActive = true
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showEnterPhoneNumberAlert() {
        let alert = UIAlertController(title: "Phone number",
                                      message: "",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) { action in
            let nameField = alert.textFields![0]
            guard let nameText = nameField.text else { return }
            if !nameText.isEmpty {
                HUD.show(.progress)
                let nameAttribute = AWSCognitoIdentityUserAttributeType()
                nameAttribute?.name = "phone_number"
                nameAttribute?.value = nameText
                AppDelegate.defaultUserPool().currentUser()?.update([nameAttribute!]).continueOnSuccessWith(block: { (task) -> Any? in
                    DispatchQueue.main.async {
                        if task.error == nil {
                            dump(task.result)
                            AppDelegate.defaultUserPool().currentUser()?.getDetails().continueOnSuccessWith(block: { (task) -> Any? in
                                guard task.result != nil else {
                                    Shared.showAlert(title: "Change phone number Error", message: (task.error?.localizedDescription)!, viewController: self)
                                    return nil
                                }
                                var userAttributes:[AWSCognitoIdentityProviderAttributeType]?
                                userAttributes = task.result?.userAttributes
                                let nameValue = userAttributes?.filter { $0.name == "phone_number" }
                                let defaults = UserDefaults.standard
                                defaults.set(nameValue?.first?.value ?? "", forKey: "PHONE")
                                defaults.synchronize()
                                DispatchQueue.main.async {
                                    HUD.hide()
                                    self.phoneNumberLabel.text = nameText
                                    self.tableView.reloadData()
                                }
                                return nil
                            })
                        } else {
                            HUD.hide()
                            Shared.showAlert(title: "Change phone number Error", message: (task.error?.localizedDescription)!, viewController: self)
                        }
                    }
                })
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        alert.addTextField { textPassword in
            textPassword.placeholder = "Enter phone number"
            textPassword.heightAnchor.constraint(equalToConstant: 25).isActive = true
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath == IndexPath(row: 0, section: 0) { // Change password
            changeUserPassword()
        } else if indexPath == IndexPath(row: 1, section: 1) { // Change name
            showChangeDisplayNameAlert()
        } else if indexPath == IndexPath(row: 3, section: 1) { // Add phone number
            showEnterPhoneNumberAlert()
        }
    }

    /*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    */
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
