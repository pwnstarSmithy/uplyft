//
//  ReminderSettingsTableViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 02/03/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import UserNotifications

class ReminderSettingsTableViewController: UITableViewController {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var onceADayCell: UITableViewCell!
    @IBOutlet weak var twiceADayCell: UITableViewCell!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissController))
        navigationItem.rightBarButtonItem = rightBarButton
        
        setUpLocalNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpLocalNotifications()
        if defaults.bool(forKey: "IS_ONCE_A_DAY") {
            self.onceADayCell.accessoryType = .checkmark
        } else if defaults.bool(forKey: "IS_TWICE_A_DAY") {
            self.twiceADayCell.accessoryType = .checkmark
        }
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let staticIndexPath = tableView.indexPath(for: self.onceADayCell), staticIndexPath == indexPath {
            self.onceADayCell.accessoryType = .checkmark
            defaults.set(true, forKey: "IS_ONCE_A_DAY")
            defaults.synchronize()
        } else {
            self.onceADayCell.accessoryType = .none
            defaults.set(false, forKey: "IS_ONCE_A_DAY")
            defaults.synchronize()
        }
        
        if let staticIndexPath = tableView.indexPath(for: self.twiceADayCell), staticIndexPath == indexPath {
            self.twiceADayCell.accessoryType = .checkmark
            defaults.set(true, forKey: "IS_TWICE_A_DAY")
            defaults.synchronize()
        } else {
            self.twiceADayCell.accessoryType = .none
            defaults.set(false, forKey: "IS_ONCE_A_DAY")
            defaults.synchronize()
        }
    }
    
    @objc func dismissController() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - Notifications
    private func setUpLocalNotifications() {
        UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
            switch notificationSettings.authorizationStatus {
            case .notDetermined:
                // Request auth
                self.requestAuthorization(completionHandler: { (success) in
                    guard success else { return }
                    
                    // Schedule local notification
                    DispatchQueue.main.async {
                        self.tableView.footerView(forSection: 0)?.isHidden = true
                    }
                    
                })
            case .authorized:
                // Schedule local notification
                print("Notifications allowed")
                DispatchQueue.main.async {
                    self.tableView.footerView(forSection: 0)?.isHidden = true
                }
            case .denied:
                print("Application Not Allowed to Display Notifications")
                DispatchQueue.main.async {
                    self.tableView.footerView(forSection: 0)?.isHidden = false
                }
            case .provisional:
                print("provisional access")
            }
        }
    }
    
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
            
            completionHandler(success)
        }
    }

    /*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    */

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
