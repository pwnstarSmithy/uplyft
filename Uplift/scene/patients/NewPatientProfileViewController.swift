//
//  NewPatientProfileViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 02/03/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit

class NewPatientProfileViewController: UIViewController {
    
    @IBAction func editProfileButtonTapped(_ sender: Any) {
    }
    
    @IBAction func settingsButtonTapped(_ sender: Any) {
        
    }
    
    @IBAction func logoutButtonTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Ok button a alert"), style: .default) { (action) in
            Shared.logout(viewController: self)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel button a alert"), style: .cancel) { (action) in
        }
        
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteUser))
        navigationItem.rightBarButtonItem = rightBarButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func deleteUser() {
        print("Delete User")
        if let currentUser = AppDelegate.defaultUserPool().currentUser(), currentUser.isSignedIn {
            currentUser.delete()
            currentUser.signOutAndClearLastKnownUser()
            //Shared.logout(viewController: self)
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.signinUser()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
