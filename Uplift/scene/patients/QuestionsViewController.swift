//
//  QuestionsViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 31/10/2017.
//  Copyright © 2017 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSDynamoDB
import AWSCognitoIdentityProvider
import SwiftDate
import UserNotifications

class QuestionsViewController: UIViewController {
    
    let defaults = UserDefaults.standard

    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView!
    
    var items: [Question] = []
    
    var selectedQuestion: Question!
    
    var patientEmail: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Configure User Notification Center
        UNUserNotificationCenter.current().delegate = self
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        if let email = defaults.string(forKey: "EMAIL") {
            patientEmail = email
        }
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USWest2,identityPoolId:"us-west-2:7fcee860-434a-4d93-8357-0231609ad65c")
        let configuration = AWSServiceConfiguration(region:.USWest2, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        //awsCognitoIdentityProvider = AWSCognitoIdentityProvider.default()
        getQuestions(isActive: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkForSettingToAllowAnswersEditByDoctor()
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        getQuestions(isActive: true)
    }
    
    private func getQuestions(isActive: Bool) {
        let scanExpression = AWSDynamoDBScanExpression()
        scanExpression.filterExpression = "active = :val AND belongsTo = :patient"
        scanExpression.expressionAttributeValues = [":val": isActive, ":patient" : patientEmail]
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
        dynamoDBObjectMapper.scan(Question.self, expression: scanExpression).continueWith { (task) -> Any? in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.refreshControl.endRefreshing()
                if let error = task.error as NSError? {
                    print("The request failed. Error: \(error)")
                } else if let paginatedOutput = task.result {
                    self.items =  paginatedOutput.items as! [Question]
                    self.items = self.items.sorted(by: { (qtn1, qtn2) -> Bool in
                        Double(qtn1.timeAdded!)! > Double(qtn2.timeAdded!)!
                    })
                    dump(self.items)
                    if self.items.count > 0 {
                        self.tableView.backgroundView = nil
                        self.setUpLocalNotifications()
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    // MARK: - Notifications
    private func setUpLocalNotifications() {
        UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
            switch notificationSettings.authorizationStatus {
            case .notDetermined:
                // Request auth
                self.requestAuthorization(completionHandler: { (success) in
                    guard success else { return }
                    
                    // Schedule local notification
                    self.scheduleLocalNotification()
                })
            case .authorized:
                // Schedule local notification
                self.scheduleLocalNotification()
            case .denied:
                print("Application Not Allowed to Display Notifications")
            case .provisional:
                print("Provisional Application Not Allowed to Display Notifications")
            }
        }
    }
    
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
            
            completionHandler(success)
        }
    }
    
    private func scheduleLocalNotification() {
        // Create Notification Content
        let notificationContent = UNMutableNotificationContent()
        
        // Configure Notification Content
        notificationContent.title = "Reminder"
        notificationContent.subtitle = "Have you answered all your questions?"
        notificationContent.body = "A reminder to provide answers to your doctors' questions."
        
        // Add Trigger
        /*let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 10.0, repeats: false)*/
        var dateComponents = DateComponents()
        
        if defaults.bool(forKey: "IS_TWICE_A_DAY") {
            dateComponents.hour = 19
            dateComponents.minute = 00
            
            let notifTrigger  = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
            
            // Create Notification Request
            let notificationRequest = UNNotificationRequest(identifier: "uplift_local_notification_daily", content: notificationContent, trigger: notifTrigger)
            
            // Add Request to User Notification Center
            UNUserNotificationCenter.current().add(notificationRequest) { (error) in
                if let error = error {
                    print("Unable to Add Notification Request daily (\(error), \(error.localizedDescription))")
                }
            }
            print("Daily notif set")
        }
        
        dateComponents.hour = 09
        dateComponents.minute = 00
        if !defaults.bool(forKey: "IS_TWICE_A_DAY") && !defaults.bool(forKey: "IS_ONCE_A_DAY") {
            print("This is set for once a day")
            defaults.set(true, forKey: "IS_ONCE_A_DAY")
            defaults.synchronize()
        }
        
        let notifTrigger  = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        // Create Notification Request
        let notificationRequest = UNNotificationRequest(identifier: "uplift_local_notification", content: notificationContent, trigger: notifTrigger)
        
        // Add Request to User Notification Center
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            if let error = error {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
            }
        }
    }
    
    private func checkForSettingToAllowAnswersEditByDoctor() {
        AppDelegate.defaultUserPool().currentUser()?.getDetails().continueOnSuccessWith(block: { (task) -> Any? in
            guard task.result != nil else {
                return nil
            }
            var userAttributes:[AWSCognitoIdentityProviderAttributeType]?
            userAttributes = task.result?.userAttributes
            let allowEditsValue = userAttributes?.filter { $0.name == "custom:allowAnswersEdit" }
            let defaults = UserDefaults.standard
            let isAllowEdit = allowEditsValue?.first?.value ?? "1"
            defaults.set(isAllowEdit == "1" ? true : false, forKey: PatientsSettings.allowPatientsToEditAnswers.rawValue)
            defaults.synchronize()
            return nil
        })
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showQuestionAskedSegue" {
            let viewQuestionVC = segue.destination as! ViewQuestionViewController
            viewQuestionVC.selectedQuestion = selectedQuestion
        } else if segue.identifier == "showAnswersSegue" {
            let answerSegue = segue.destination as! AnsweredQuestionViewController
            answerSegue.selectedQuestion = selectedQuestion
            answerSegue.shouldAddNewAnswerButton = true
            answerSegue.patientEmail = AppDelegate.defaultUserPool().currentUser()?.username ?? ""
        }
    }
    

}

extension QuestionsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedQuestion = items[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "showAnswersSegue", sender: self)
    }
}

extension QuestionsViewController: UITableViewDataSource { // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.items.count == 0 {
            let message = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
            message.text = "No questions found"
            message.textAlignment = NSTextAlignment.center
            message.sizeToFit()
            //set tableview background
            self.tableView.backgroundView = message
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientQuestionCell", for: indexPath) as! PatientQuestionCell
        cell.questionTextLabel?.text = items[indexPath.row].name
        let date = DateInRegion(NSDate(timeIntervalSince1970: Double(items[indexPath.row].timeAdded!)!) as Date, region: Region.currentIn())
        //let (colloquial, _) = try! date.colloquialSinceNow()
        cell.footnoteLabel?.text = "Asked: \(date.toRelative())"
        return cell
    }
}

extension QuestionsViewController: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
}
