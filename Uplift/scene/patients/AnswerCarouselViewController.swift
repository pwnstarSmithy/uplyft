//
//  AnswerCarouselViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 01/01/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import AWSDynamoDB
import AWSCognitoIdentityProvider
import Firebase
import RealmSwift
import PKHUD

class AnswerCarouselViewController: UIViewController {
    
    var awsCognitoIdentityProvider:AWSCognitoIdentityProvider?
    
    var patientAttributes:AWSCognitoIdentityUserGetDetailsResponse?
    
    let defaults = UserDefaults.standard
    
    var items: [Question] = [Question]()
    
    var questionKeys:[String] = [String]()
    
    var pageViewController : UIPageViewController!
    
    var pageContentViewController : UIViewController?
    
    var count = 0
    
    var changeButtonToSubmit = false
    
    var patientEmail: String!
    
    var isAllowedToEditAnswers = true
    
    @IBOutlet weak var singleNextButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var doubleButtonContainer: UIView!
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        changePage(direction: .forward)
    }
    @IBAction func previousButtonTapped(_ sender: Any) {
        changePage(direction: .reverse)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USWest2,identityPoolId:"us-west-2:7fcee860-434a-4d93-8357-0231609ad65c")
        let configuration = AWSServiceConfiguration(region:.USWest2, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        awsCognitoIdentityProvider = AWSCognitoIdentityProvider.default()
        
        if let email = defaults.string(forKey: "EMAIL") {
            patientEmail = email
        }
        AppDelegate.defaultUserPool().currentUser()?.getDetails().continueWith { (task) -> Any? in
            dump(task)
            if task.error != nil {
            } else {
                self.patientAttributes = task.result
                dump(self.patientAttributes?.userAttributes)
            }
            return nil
        }
        getQuestions(isActive: true)
        reset()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isAllowedToEditAnswers = defaults.bool(forKey: PatientsSettings.allowPatientsToEditAnswers.rawValue)
    }
    
    private func getQuestions(isActive: Bool) {
        let scanExpression = AWSDynamoDBScanExpression()
        scanExpression.filterExpression = "active = :val AND belongsTo = :patient"
        scanExpression.expressionAttributeValues = [":val": isActive, ":patient" : patientEmail]
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
        dynamoDBObjectMapper.scan(Question.self, expression: scanExpression).continueWith { (task) -> Any? in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let error = task.error as NSError? {
                    print("The request failed. Error: \(error)")
                } else if let paginatedOutput = task.result {
                    self.items =  paginatedOutput.items as! [Question]
                    self.items = self.items.sorted(by: { (qtn1, qtn2) -> Bool in
                        Double(qtn1.timeAdded!)! > Double(qtn2.timeAdded!)!
                    })
                    dump(self.items)
                    if self.items.count > 0 {
                        self.reset()
                    }
                }
            }
        }
    }
    
    func reset() {
        /* Getting the page View controller */
        pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! UIPageViewController
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        
        pageContentViewController = self.viewControllerAtIndex(index: 0)
        if let pageContentViewController = pageContentViewController {
            self.pageViewController.setViewControllers([pageContentViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
            
            /* We are substracting 150 because we have the prev & next buttons whose height is 90*/
            self.pageViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - (UIDevice.isIphoneX ? 180 : 150))
            self.addChildViewController(pageViewController)
            self.view.addSubview(pageViewController.view)
            self.pageViewController.didMove(toParentViewController: self)
        }
        
        doubleButtonContainer.isHidden = true
        reinstateSingleNextButton()
        reinstateNextButton()
    }
    
    func viewControllerAtIndex(index : Int) -> UIViewController? {
        if((self.items.count <= 0) || (index >= self.items.count)) {
            return nil
        }
        
        let pageContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageContentViewController") as! PageContentViewController
    
        pageContentViewController.questionKey = self.items[index].uuid!
        pageContentViewController.questionText = self.items[index].name
        pageContentViewController.answerText = self.items[index].questionText
        pageContentViewController.pageIndex = index
        return pageContentViewController
    }
    
    func changePage(direction: UIPageViewControllerNavigationDirection){
        var isLastViewControllerDisplayed = false
        if ((pageViewController.viewControllers?.first as? PageContentTableViewController) != nil) {
            if direction == .forward {
                confirmSubmissionOfAnswers()
                return
            }
            isLastViewControllerDisplayed = true
            pageViewController.setViewControllers([viewControllerAtIndex(index: (items.count-1))!], direction: direction, animated: true, completion: nil)
            reinstateNextButton()
        }
        
        guard let viewController = pageViewController.viewControllers?.first as? PageContentViewController, var viewControllerIndex = viewController.pageIndex else {
            return
        }
        if direction == .forward {
            viewControllerIndex = viewControllerIndex + 1
        } else if !isLastViewControllerDisplayed {
            viewControllerIndex = viewControllerIndex - 1
        }
        
        if viewControllerIndex == 0 {
            doubleButtonContainer.isHidden = true
        } else {
            setUpPrevNextButtons()
        }
        
        if let pageContentVC = viewControllerAtIndex(index: viewControllerIndex) {
            pageViewController.setViewControllers([pageContentVC], direction: direction, animated: true, completion: nil)
            reinstateNextButton()
        } else if viewControllerIndex >= items.count {
            pageViewController.setViewControllers([pageContentInTableView()!], direction: .forward, animated: true, completion: nil)
            print("isAllowedToEditAnswers ---> \(isAllowedToEditAnswers)")
            if isAllowedToEditAnswers {
                changeNextButtonToSubmit()
            } else {
                removePreviousButtonForNoAnswerEditsAllowed()
            }
        }
        
    }
    
    private func setUpPrevNextButtons() {
        doubleButtonContainer.backgroundColor = UIColor.white
        doubleButtonContainer.isHidden = false
    }
    
    private func changeNextButtonToSubmit() {
        nextButton.setTitle("SUBMIT", for: .normal)
        nextButton.backgroundColor = Shared.hexStringToUIColor(hex: "4CAF50")
    }
    
    private func reinstateNextButton() {
        nextButton.setTitle("NEXT", for: .normal)
        nextButton.backgroundColor = Shared.hexStringToUIColor(hex: "3093FF")
    }
    
    private func reinstateSingleNextButton() {
        singleNextButton.setTitle("NEXT", for: .normal)
        singleNextButton.backgroundColor = Shared.hexStringToUIColor(hex: "3093FF")
    }
    private func removePreviousButtonForNoAnswerEditsAllowed() {
        doubleButtonContainer.isHidden = true
        singleNextButton.setTitle("SUBMIT", for: .normal)
        singleNextButton.backgroundColor = Shared.hexStringToUIColor(hex: "4CAF50")
    }
    
    private func pageContentInTableView()  -> UIViewController? {
        let pageContentTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageContentTableViewController") as! PageContentTableViewController
        pageContentTableViewController.items = self.items
        return pageContentTableViewController
    }
    
    private func confirmSubmissionOfAnswers() {
        let alert = UIAlertController(title: "Submit Answers",
                                      message: "Are you sure you want to submit your answers?",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Submit", style: .default) { action in
            print("Submit items")
            // disable paging here and loop through all saved answers and submit them for storage\
            
            // Here post a notification to save any answers that have not been saved in case they are entered
            // using the PageContentTableViewController
            NotificationCenter.default.post(name: Notification.Name("SaveAnyUnSavedAnswersEntered"), object: nil)
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            HUD.show(.progress)
            do {
                let realm = try Realm()
                let offlineAnswers =  realm.objects(OfflineAnswer.self).filter("answer != ''")
                var counter = 0
                if offlineAnswers.count > 0 {
                    var writeRequests: [AWSDynamoDBWriteRequest] = []
                    for answer in offlineAnswers {
                        let nsdateAdded = NSDate().timeIntervalSince1970
                        
                        let uuid = AWSDynamoDBAttributeValue()
                        uuid?.s = NSUUID().uuidString.lowercased()
                        let name = AWSDynamoDBAttributeValue()
                        name?.s = answer.answer
                        let addedBy = AWSDynamoDBAttributeValue()
                        addedBy?.s = AppDelegate.defaultUserPool().currentUser()?.username ?? ""
                        let belongsToQuestion = AWSDynamoDBAttributeValue()
                        belongsToQuestion?.s = answer.questionKey
                        let timeAdded = AWSDynamoDBAttributeValue()
                        timeAdded?.s = "\(nsdateAdded)"
                        let isActive = AWSDynamoDBAttributeValue()
                        isActive?.boolean = true
                        
                        let answerParams = [
                            "uuid": uuid!,
                            "name": name!,
                            "addedBy" : addedBy!,
                            "belongsToQuestion": belongsToQuestion!,
                            "active": isActive!,
                            "timeAdded": timeAdded!,
                        ]
                        let writeRequest = AWSDynamoDBWriteRequest()
                        writeRequest?.putRequest = AWSDynamoDBPutRequest()
                        writeRequest?.putRequest?.item = answerParams
                        writeRequests.append(writeRequest!)
                    }
                    
                    let batchWriteItemInput = AWSDynamoDBBatchWriteItemInput()
                    batchWriteItemInput?.requestItems = ["Answers": writeRequests]
                    let dynamoDB = AWSDynamoDB.default()
                    dynamoDB.batchWriteItem(batchWriteItemInput!) { (aWSDynamoDBBatchWriteItemOutput, error) in
                        DispatchQueue.main.async {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            if let error = error {
                                HUD.flash(.error, delay: 3.0)
                                print("The request failed. Error: \(error)")
                            } else {
                                
                                
                                //Delete submitted answers
                                if realm.isInWriteTransaction {
                                    realm.delete(offlineAnswers)
                                    // post a notification to alert the content table to reload and clear the input data since it has been successfully submitted
                                    NotificationCenter.default.post(name: Notification.Name("ReloadTableViewOnClearingSavedAnswers"), object: nil)
                                } else {
                                    try! realm.write {
                                        realm.delete(offlineAnswers)
                                        // post a notification to alert the content table to reload and clear the input data since it has been successfully submitted
                                        NotificationCenter.default.post(name: Notification.Name("ReloadTableViewOnClearingSavedAnswers"), object: nil)
                                    }
                                }
                                
                                // set patient and question flags for new answers
                                if let pat = self.patientAttributes {
                                    self.updatePatientNewAnswerFlags()
                                    self.setNotification(patient: pat)
                                    HUD.flash(.success, delay: 2.0)
                                    self.reset()
                                }
                            }
                        }
                    }

                    
                    /*for answer in offlineAnswers {
                        Shared.saveAnswer(text: answer.answer, questionKey: answer.questionKey) { key in
                            print(key)
                            counter = counter + 1
                            for question in self.items {
                                if question.uuid! == key {
                                    question.isNewAnswer = true
                                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                                    let dynamoDBObjectMapper = AWSDynamoDBObjectMapper.default()
                                    dynamoDBObjectMapper.save(question).continueWith { (task) -> Any? in
                                        DispatchQueue.main.async {
                                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                            if let error = task.error as NSError? {
                                                print("The request failed. Error: \(error)")
                                            }
                                        }
                                    }
                                }
                            }
                            if counter == offlineAnswers.count {
                                //Delete submitted answers
                                if realm.isInWriteTransaction {
                                    realm.delete(offlineAnswers)
                                    // post a notification to alert the content table to reload and clear the input data since it has been successfully submitted
                                    NotificationCenter.default.post(name: Notification.Name("ReloadTableViewOnClearingSavedAnswers"), object: nil)
                                } else {
                                    try! realm.write {
                                        realm.delete(offlineAnswers)
                                        // post a notification to alert the content table to reload and clear the input data since it has been successfully submitted
                                        NotificationCenter.default.post(name: Notification.Name("ReloadTableViewOnClearingSavedAnswers"), object: nil)
                                    }
                                }
                                
                                // set patient and question flags for new answers
                                if let pat = self.patientAttributes {
                                    self.updatePatientNewAnswerFlags()
                                    self.setNotification(patient: pat)
                                    HUD.flash(.success, delay: 2.0)
                                }
                            }
                        }
                    }*/
                    
                } else {
                    HUD.flash(.labeledError(title: "Submit", subtitle: "No answers were found"), delay: 3.0)
                }
            } catch let error {
                print(error)
                HUD.flash(.labeledError(title: "Submit", subtitle: "Unable to submit. Try again"), delay: 3.0)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func setNotification(patient: AWSCognitoIdentityUserGetDetailsResponse) {
        let tokens = patient.userAttributes?.filter { $0.name == "custom:deviceTokens" }
        let tokenValues = tokens?.first?.value
        let name = patient.userAttributes?.filter { $0.name == "name" }
        let notData: [String: Any] = [
            "to" : tokenValues?.components(separatedBy: ",").last ?? "",
            "notification": [
                "title" : "New Answer Received",
                "body"  : "✨ A new answer has been submited by your patient \(name?.first?.value ?? "") ✍️"
                ] as Dictionary
        ]
        Shared.sendPushNotification(notificationData: notData)
    }
    
    func updatePatientNewAnswerFlags() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let req = AWSCognitoIdentityProviderAdminUpdateUserAttributesRequest()!
        req.username = AppDelegate.defaultUserPool().currentUser()?.username!
        req.userPoolId = awsPoolID
        req.userAttributes = [AWSCognitoIdentityUserAttributeType(name: "custom:newAnswersAdded", value: "1")]
        awsCognitoIdentityProvider?.adminUpdateUserAttributes(req, completionHandler: { (response, error) in
            if error == nil {
            } else {
                print("The request failed. Error: \(String(describing: error))")
            }
        })
    }
    
    func updateQuestionsWithNewAnswers(offlineAnswers: [OfflineAnswer]) {
        
        var questionsToUpdate = [Question]()
        for answer in offlineAnswers {
            let questions = self.items.filter { (question) in
                question.uuid == answer.questionKey
            }
            if questions.count > 0 {
                questionsToUpdate.append(questions.first!)
            }
        }
        
        if questionsToUpdate.count > 0 {
            
            let dynamoDB = AWSDynamoDB.default()
            let updateInput = AWSDynamoDBUpdateItemInput()
            let isNewAnswerValue = AWSDynamoDBAttributeValue()
            isNewAnswerValue?.boolean = true
            updateInput?.tableName = "Questions"
            updateInput?.key = ["uuid" : isNewAnswerValue!]
            
            let oldIsNewPrice = AWSDynamoDBAttributeValue()
            oldIsNewPrice?.boolean = false
            
            let expectedValue = AWSDynamoDBExpectedAttributeValue()
            expectedValue?.value = oldIsNewPrice
            
            let isNewAnswer = AWSDynamoDBAttributeValue()
            isNewAnswer?.boolean = true
            
            let valueUpdate = AWSDynamoDBAttributeValueUpdate()
            valueUpdate?.value = isNewAnswer
            valueUpdate?.action = .put
            
            updateInput?.attributeUpdates = ["isNewAnswer": valueUpdate!]
            updateInput?.expected = ["isNewAnswer": expectedValue!]
            updateInput?.returnValues = .updatedNew
            //dynamoDB.updateItem(AWSDynamoDBUpdateItemInput)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension AnswerCarouselViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var isLastViewControllerDisplayed = false
        
        if ((self.pageViewController.viewControllers?.first as? PageContentTableViewController) != nil) {
            self.pageViewController.setViewControllers([viewControllerAtIndex(index: (items.count-1))!], direction: .forward, animated: true, completion: nil)
            isLastViewControllerDisplayed = true
        }
        
        if ((viewController as? PageContentTableViewController) != nil) {
            return viewControllerAtIndex(index: items.count - 1)
        }
        
        guard let viewControllerIndex = (viewController as! PageContentViewController).pageIndex else {
            return nil
        }
        
        if viewControllerIndex == 0 {
            doubleButtonContainer.isHidden = true
        }
        
        let previousIndex = isLastViewControllerDisplayed ? viewControllerIndex : viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard items.count > previousIndex else {
            return nil
        }
        
        return viewControllerAtIndex(index: previousIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if ((pageViewController.viewControllers?.first as? PageContentTableViewController) != nil) {
            return nil
        }
        
        if ((viewController as? PageContentTableViewController) != nil) {
            return nil
        }
        
        guard let viewControllerIndex = (viewController as! PageContentViewController).pageIndex else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        
        let orderedViewControllersCount = items.count
        
        guard orderedViewControllersCount != nextIndex else {
            return pageContentInTableView()
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        setUpPrevNextButtons()
        
        return viewControllerAtIndex(index: nextIndex)
    }
    
    
}

extension AnswerCarouselViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if previousViewControllers.first! is PageContentTableViewController {
            reinstateNextButton()
            changeButtonToSubmit = false
        } else if changeButtonToSubmit && finished && completed {
            setUpPrevNextButtons()
            print("isAllowedToEditAnswers ---> \(isAllowedToEditAnswers)")
            if isAllowedToEditAnswers {
                changeNextButtonToSubmit()
            } else {
                removePreviousButtonForNoAnswerEditsAllowed()
            }
            
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if pendingViewControllers.first! is PageContentTableViewController {
            changeButtonToSubmit = true
        } else {
            changeButtonToSubmit = false
        }
        
    }
}



