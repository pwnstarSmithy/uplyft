//
//  PageContentViewController.swift
//  Uplift
//
//  Created by Harold Asiimwe on 01/01/2018.
//  Copyright © 2018 Harold Asiimwe. All rights reserved.
//

import UIKit
import RealmSwift
import GrowingTextView


class PageContentViewController: UIViewController {
    
    @IBOutlet weak var questionHeading: UILabel!
    @IBOutlet weak var answerInputTextView: GrowingTextView!
    @IBOutlet weak var bottomTextViewConstraint: NSLayoutConstraint!
    
    var pageIndex: Int?
    var questionText : String!
    var answerText : String!
    var questionKey: String!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let height = UIDevice.isIphoneX ? UIScreen.main.bounds.height - (72 + 260) : UIScreen.main.bounds.height - (87 + 260)
        setTextViewHeight(height: height, constraintHeight: 10)
        
        self.questionHeading.text = self.questionText
        self.answerInputTextView.placeholderColor = UIColor.gray
        self.answerInputTextView.placeholder = self.answerText.isEmpty ? "Enter your answer here" : self.answerText
        self.questionHeading.alpha = 0.1
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.questionHeading.alpha = 1.0
        })
        self.hideKeyboardWhenTappedAround()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.answerInputTextView.text = Shared.retrieveOfflineAnswer(questionKey: self.questionKey).isEmpty ? "" : Shared.retrieveOfflineAnswer(questionKey: self.questionKey)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        /// Save any input that has been entered by the user locally here
        do {
            let realm = try Realm()
            let answerToBeSaved = OfflineAnswer()
            answerToBeSaved.questionKey = self.questionKey
            answerToBeSaved.answer = self.answerInputTextView.text
            
            try! realm.write {
                realm.add(answerToBeSaved, update: true)
            }
            
        } catch let error {
            print(error)
        }
    }
    
    private func setTextViewHeight(height: CGFloat, constraintHeight: CGFloat) {
        //let height = UIDevice.isIphoneX ? UIScreen.main.bounds.height - (72 + 260) : UIScreen.main.bounds.height - (72 + 200)
        //let height = UIScreen.main.bounds.height / 3
        answerInputTextView.minHeight = height
        answerInputTextView.maxHeight = height
        self.bottomTextViewConstraint.constant = constraintHeight
        self.answerInputTextView.layoutIfNeeded()
        self.view.layoutIfNeeded()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        DispatchQueue.main.async {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                let height = UIDevice.isIphoneX ?  keyboardSize.height + 82 : keyboardSize.height + 62
                self.setTextViewHeight(height: height, constraintHeight: keyboardSize.height-55)
            }
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        DispatchQueue.main.async {
            let height = UIDevice.isIphoneX ? UIScreen.main.bounds.height - (72 + 260) : UIScreen.main.bounds.height - (72 + 260)
            self.setTextViewHeight(height: height, constraintHeight: 10)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
